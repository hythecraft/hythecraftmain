package com.hythecraft.hythemain.config;

public class Language {
	public static String COULD_NOT_PARSE_GENERIC = "&c%s is not a number.";
	
	public static String NO_PERMISSION_GENERIC = "&cYou do not have permission for this command!";
	public static String PLAYER_ONLY_GENERIC = "&cOnly players can execute this command!";
	public static String CONSOLE_ONLY_GENERIC = "&cOnly console can execute this command!";
	
	public static String CANNOT_CONVERT_UUID = "Cannot convert %s to UUID";
	public static String ARGS_MUST_BE_NUMERIC = "&cArgument(s) must be numeric!";
	public static String ARGS_TOO_MANY = "&cToo many arguments!";
	public static String ARGS_NOT_ENOUGH = "&cNot enough arguments!";
	public static String ARGS_INCORRECT = "&cIncorrect arguments";
	
	public static String NO_SUCH_PLAYER_NAME = "&cCannot find player with name %s";
	public static String NO_SUCH_PLAYER_UUID = "&cCannot find player with uuid %s";
	
	public static String NO_SUCH_ENTITY_ID = "&cCannot find entity with id %s";
	
	public static String NO_SUCH_CUSTOM_ITEM_ID = "&cCannot find item with id %s";
	
	public static String NO_SUCH_QUEST_ID = "&cCannot find quest with id %s";
	public static String NO_SUCH_DIALOGUE_ID = "&cCannot find dialogue with id %s";
	public static String NO_SUCH_PARTICLE_ID = "&cCannot find particle effect with id %s";
	public static String NO_SUCH_GROUP_ID = "&cCannot find group with id %s";
	public static String NO_SUCH_MODIFIER_ID = "&cCannot find mod with id %s";
	
	public static String PARTY_PREFIX = "&7&l[&6Party&7&l] ";
	public static String PARTY_DISBANDED = "&cParty disbanded.";
	
	public static String PARTY_PLAYER_LEFT = " &7has left the party.";
	public static String PARTY_KICKED_PARTY = " &7has been kicked from the party.";
	public static String PARTY_KICKED_YOU = "&7You have been kicked from the party.";
	public static String PARTY_PLAYER_JOIN = " &7has joined the party.";
	public static String PARTY_FULL = "&cThe party is full!";
	public static String PARTY_CREATED_SUCCESSFULLY = "&aParty created successfully.";
	public static String PARTY_CANT_KICK_SELF = "&cYou can't kick yourself!";
	public static String PLAYER_ALREADY_IN_PARTY = "&cPlayer already in party";
	public static String PLAYER_PARTY_INVITE_DISABLED = "&cPlayer has party invites disabled";
	public static String PARTY_CANT_LEAVE_AS_LEADER = "&cYou cannot leave the party as a leader!";
	
	public static String CANNOT_INVITE_PLAYER = "&cYou cannot invite this player right now.";
	public static String CANT_INVITE_SELF = "&cYou can't invite yourself!";
	public static String ALREADY_IN_PARTY = "&cYou are already in a party!";
	public static String NO_PARTY_REQUEST = "&cYou do not have any party invites!";
	public static String NOT_IN_PARTY = "&cYou are not in a party!";
	public static String NOT_PARTY_LEADER = "&cYou are not the leader of your party!";
	
	public static String NO_TRADE_REQUEST = "&cYou do not have a trade request";
	public static String TRADE_REQUEST_ALREADY_SENT = "&cYou already have a pending trade request";
	public static String TRADE_CANCELLED = "&cTrade cancelled";
	public static String TRADE_ACCEPTED = "&aTrade accepted";
	
}
