package com.hythecraft.hythemain.util.location;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.util.unicode.CharRepo;

public class LocationUtil {

	public static boolean withinBounds(Location obj, Location loc1, Location loc2) {
		double minX, maxX;
		double minY, maxY;
		double minZ, maxZ;
		minX = Math.min(loc1.getX(), loc2.getX());
		maxX = Math.max(loc1.getX(), loc2.getX());
		
		minY = Math.min(loc1.getY(), loc2.getY());
		maxY = Math.max(loc1.getY(), loc2.getY());
		
		minZ = Math.min(loc1.getZ(), loc2.getZ());
		maxZ = Math.max(loc1.getZ(), loc2.getZ());
		
		if(obj.getX() > maxX || obj.getX() < minX)
			return false;
		if(obj.getY() > maxY || obj.getY() < minY)
			return false;
		if(obj.getZ() > maxZ || obj.getZ() < minZ)
			return false;
	
		return true;		
	}
	
	//disregards z axis
	public static boolean withinBounds(Location obj, Vector2 loc1, Vector2 loc2) {
		World w = HytheCraft.getDefaultWorld();
		return withinBounds(obj, new Location(w, loc1.getX(), 256, loc1.getZ()), new Location(w, loc2.getX(), -256, loc2.getZ()));
	}
	

	//disregards z axis
	public static boolean withinBounds(Vector2 obj, Vector2 loc1, Vector2 loc2)
	{
		World w = HytheCraft.getDefaultWorld();
		return withinBounds(new Location(w, obj.getX(), 0, obj.getZ()), loc1, loc2);
		
	}
	
	public static String getDirection(Player player) {
		double rotation = (player.getLocation().getYaw() - 90.0F) % 360.0F;
        if (rotation < 0.0D) {
            rotation += 360.0D;
        }
        if ((0.0D <= rotation) && (rotation < 22.5D)) {
            return "W";
        }
        if ((22.5D <= rotation) && (rotation < 67.5D)) {
            return "NW";
        }
        if ((67.5D <= rotation) && (rotation < 112.5D)) {
            return "N";
        }
        if ((112.5D <= rotation) && (rotation < 157.5D)) {
            return "NE";
        }
        if ((157.5D <= rotation) && (rotation < 202.5D)) {
            return "E";
        }
        if ((202.5D <= rotation) && (rotation < 247.5D)) {
            return "SE";
        }
        if ((247.5D <= rotation) && (rotation < 292.5D)) {
            return "S";
        }
        if ((292.5D <= rotation) && (rotation < 337.5D)) {
            return "SW";
        }
        if ((337.5D <= rotation) && (rotation < 360.0D)) {
            return "W";
        }
        return "";
	}
	
	public static CharRepo directionAsChar(Player player) {
		String dir = getDirection(player);
		switch(dir)
		{
		case "N":
			return CharRepo.COMPASS_NORTH;
		case "NW":
			return CharRepo.COMPASS_NORTH_WEST;
		case "W":
			return CharRepo.COMPASS_WEST;
		case "NE":
			return CharRepo.COMPASS_NORTH_EAST;
		case "E":
			return CharRepo.COMPASS_EAST;
		case "SE":
			return CharRepo.COMPASS_SOUTH_EAST;
		case "S":
			return CharRepo.COMPASS_SOUTH;
		case "SW":
			return CharRepo.COMAPSS_SOUTH_WEST;		
		}
		return CharRepo.COMPASS_NORTH;
		
	}
	
	
	
}







