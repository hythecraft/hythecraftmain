package com.hythecraft.hythemain.util.entity;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R1.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.util.NumberUtil;

import net.minecraft.server.v1_16_R1.ChatMessage;
import net.minecraft.server.v1_16_R1.EntityArmorStand;
import net.minecraft.server.v1_16_R1.EntityTypes;
import net.minecraft.server.v1_16_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_16_R1.PacketPlayOutEntityMetadata;
import net.minecraft.server.v1_16_R1.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_16_R1.WorldServer;

public class HologramText {

	public HologramText(String display, LivingEntity entity, Player player) {	
		this(display, entity, 0, player);
	}
	
	public HologramText(String display, LivingEntity entity, float range, Player player) {
		
		Location loc = entity.getLocation().clone();
		WorldServer s = ((CraftWorld) loc.getWorld()).getHandle();
		EntityArmorStand stand = new EntityArmorStand(EntityTypes.ARMOR_STAND, s);
		
		stand.setLocation(loc.getX() + NumberUtil.random(-range, range), loc.getY(), loc.getZ() + NumberUtil.random(-range, range), 0, 0);
		stand.setCustomName(new ChatMessage(ChatColor.translateAlternateColorCodes('&', display)));
		stand.setCustomNameVisible(true);
		stand.setArms(false);
		stand.setInvisible(true);
		stand.setNoGravity(true);
		stand.setInvulnerable(true);
		
		PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(stand);
		((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
		  PacketPlayOutEntityMetadata p = new PacketPlayOutEntityMetadata(stand.getId(), stand.getDataWatcher(), false);
		  ((CraftPlayer)player).getHandle().playerConnection.sendPacket(p);
		  HytheCraft.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(HytheCraft.getInstance(), new Runnable() {
			  public void run() {
				  PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(stand.getId());
				  ((CraftPlayer)player).getHandle().playerConnection.sendPacket(packet);
			  }
			}, 100L);
	}
	
}
