package com.hythecraft.hythemain.util.unicode;

import lombok.Getter;

public enum ProfileRepo {
	
	WANDERING_TRADER(
			CharRepo.TRADER_FRAME_1.getCh() + CharRepo.INSET.getCh() + CharRepo.TRADER_FRAME_2.getCh() + CharRepo.INSET.getCh() + CharRepo.TRADER_FRAME_3.getCh() + CharRepo.INSET.getCh() + CharRepo.TRADER_FRAME_4.getCh(),
			CharRepo.TRADER_FRAME_5.getCh() + CharRepo.INSET.getCh() + CharRepo.TRADER_FRAME_6.getCh() + CharRepo.INSET.getCh() + CharRepo.TRADER_FRAME_7.getCh() + CharRepo.INSET.getCh() + CharRepo.TRADER_FRAME_8.getCh(),
			CharRepo.TRADER_FRAME_9.getCh() + CharRepo.INSET.getCh() + CharRepo.TRADER_FRAME_10.getCh() + CharRepo.INSET.getCh() + CharRepo.TRADER_FRAME_11.getCh() + CharRepo.INSET.getCh() + CharRepo.TRADER_FRAME_12.getCh(),
			CharRepo.TRADER_FRAME_13.getCh() + CharRepo.INSET.getCh() + CharRepo.TRADER_FRAME_14.getCh() + CharRepo.INSET.getCh() + CharRepo.TRADER_FRAME_15.getCh() + CharRepo.INSET.getCh() + CharRepo.TRADER_FRAME_16.getCh()
			);
	
	@Getter private String[] block;
	
	private ProfileRepo(String... block) {
		this.block = block;
	}
	
}
