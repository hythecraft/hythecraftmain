package com.hythecraft.hythemain.util.unicode;

import lombok.Getter;

public enum CharRepo {
	
	COMPASS_NORTH("\uE001"),
	COMPASS_NORTH_EAST("\uE005"),
	COMPASS_NORTH_WEST("\uE006"),
	
	COMPASS_EAST("\uE002"),
	COMPASS_SOUTH_EAST("\uE008"),
	COMAPSS_SOUTH_WEST("\uE007"),
	
	COMPASS_SOUTH("\uE003"),
	COMPASS_WEST("\uE004"),
	
	INSET("\uF801"),
	
	TRADER_FRAME_1("\uF000"),
	TRADER_FRAME_2("\uF001"),
	TRADER_FRAME_3("\uF002"),
	TRADER_FRAME_4("\uF003"),
	TRADER_FRAME_5("\uF004"),
	TRADER_FRAME_6("\uF005"),
	TRADER_FRAME_7("\uF006"),
	TRADER_FRAME_8("\uF007"),
	TRADER_FRAME_9("\uF008"),
	TRADER_FRAME_10("\uF009"),
	TRADER_FRAME_11("\uF010"),
	TRADER_FRAME_12("\uF011"),
	TRADER_FRAME_13("\uF012"),
	TRADER_FRAME_14("\uF013"),
	TRADER_FRAME_15("\uF014"),
	TRADER_FRAME_16("\uF015");
	
	
	private CharRepo(String unicode)
	{
		this.ch = unicode;
	}
	
	@Getter private String ch;
	
}
