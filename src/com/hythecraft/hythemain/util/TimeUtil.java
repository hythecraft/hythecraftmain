package com.hythecraft.hythemain.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class TimeUtil {
	
	static DateFormat format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
	
	public static Calendar getCal() {
		Calendar cal = Calendar.getInstance();
		return cal;
	}
	
	public static Date getDateAheadHours(int ahead) {
		Calendar clone = (Calendar) getCal().clone();
		clone.add(Calendar.HOUR_OF_DAY, ahead);
		return clone.getTime();
	}
	
	public static String toString(Date date) {
		return format.format(date);
	}
	
	public static Date fromString(String str)
	{
		try {
			return format.parse(str);
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
	    long diffInMillies = date2.getTime() - date1.getTime();
	    return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}
	
	public static String displayTime(long diff) {
		
		long remainder =  diff;
		int days = NumberUtil.roundDown(remainder / 86400, 1);
		remainder -=  days * 86400;
		
		int hours = NumberUtil.roundDown(remainder / 3600, 1);
		remainder -= hours * 3600;
		
		int minutes = NumberUtil.roundDown(remainder / 60, 1);
		remainder -= minutes * 60;
		
		int seconds = (int)remainder;
		
		return days + "d " + hours + "h " + minutes + "m " + seconds + "s";
		
	}
}













