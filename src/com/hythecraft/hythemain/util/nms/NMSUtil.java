package com.hythecraft.hythemain.util.nms;

import org.bukkit.Bukkit;

public class NMSUtil {
	
	public static Object NBTTagCompound;
	public static Object NBTTagList;
	public static Object NBTTagString;
	public static Object ItemStack;
	
	
	public static Object CraftItemStack;
	static {
		try {
		NBTTagCompound = getNMSClass("NBTTagCompound");
		NBTTagList = getNMSClass("NBTTagList");
		NBTTagString = getNMSClass("NBTTagString");
		ItemStack = getNMSClass("ItemStack");
		
		CraftItemStack = getCraftBukkitClass(".inventory.CraftItemStack");
		}catch(Exception e) {
			
		}
	}
	
	public static Class<?> getNMSClass(String name) throws ClassNotFoundException {
	   return Class.forName("net.minecraft.server." + Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3] + "." + name);
	}
	
	public static Class<?> getCraftBukkitClass(String name) throws ClassNotFoundException{
		return Class.forName("org.bukkit.craftbukkit." + Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3] + "." + name);
	}
}
