package com.hythecraft.hythemain.util;

import java.lang.reflect.Array;

public class ArrayUtil {
    @SuppressWarnings("unchecked")
	public static <E> E[] removeElement(E[] arr, int index) {
        if (arr == null ||
            index < 0 ||
            index >= arr.length) {

            return arr;
        }

        E[] anotherArray = (E[]) Array.newInstance(arr.getClass().getComponentType(), arr.length - 1);
        for (int i = 0, k = 0; i < arr.length; i++) {
            if (i == index) {
                continue;
            }
            anotherArray[k++] = arr[i];
        }

        return anotherArray;
    }
}
