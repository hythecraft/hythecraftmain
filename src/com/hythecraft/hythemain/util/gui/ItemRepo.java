package com.hythecraft.hythemain.util.gui;

import org.bukkit.Material;

public enum ItemRepo {
	
	TICK(Material.COCOA_BEANS),
	QUESTION_MARK(Material.COD_BUCKET),
	CROSS(Material.CHORUS_FRUIT),
	RELOAD(Material.FURNACE_MINECART),
	PLUS(Material.COMMAND_BLOCK_MINECART),
	BACK(Material.BARRIER),
	TRADE(Material.HOPPER_MINECART);
	
	
	private ItemRepo(Material mat)
	{
		this.mat = mat;
	}
	
	public Material mat;
	
}
