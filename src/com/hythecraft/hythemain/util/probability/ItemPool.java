package com.hythecraft.hythemain.util.probability;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.ItemManager;
import com.hythecraft.hythemain.core.items.types.CustomItem;
import com.hythecraft.hythemain.util.NumberUtil;

public class ItemPool {
	
	ProbabilityCollection<ItemStack> coll = new ProbabilityCollection<ItemStack>();
	
	public ItemPool addItem(ItemStack item, Integer prob) {
		coll.add(item, prob);
		return this;
	}
	
	public ItemPool addItem(CustomItem item, Integer prob) {
		coll.add(item.getNewStack(null), prob);
		return this;
	}
	
	public ItemPool addItem(CustomItemType item, Integer prob) {
		coll.add(ItemManager.getCustomItemHandler(item).getNewStack(null), prob);
		return this;
	}
	
	public List<ItemStack> generate(){
		int itemAmount = (int) Math.round(-(1/1.3) * NumberUtil.log(NumberUtil.random(1, 100), 2) + 6.1 );

		List<ItemStack> ret = new ArrayList<>();
		for(int i = 0; i < itemAmount; i++)
		{
			ret.add(coll.get());
		}
		
		return ret;
	}
}
