package com.hythecraft.hythemain.hook.citizens.traits.dialoguetrait.util;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.quests.util.QuestManager;

import lombok.Getter;
import lombok.Setter;
import net.citizensnpcs.api.persistence.Persist;

public class DialogueSet {
	@Persist @Getter @Setter private String dialogueID;
	@Persist @Getter @Setter private String questID; //Quest required to display dialogue (Meaning Player can START quest)
	
	@Persist @Getter @Setter private String activeDialogueID; //Displayed when player is currently doing quest
	
	public boolean isApplicable(Player player) {
		if(questID == null) return false;
		QuestManager quest = HytheCraft.getInstance().getQuestManager();
		
		if(quest.canStartQuest(questID, player)) return true;
		if(quest.inProgressQuest(questID, player)) return true;
		return false;
	}
}
