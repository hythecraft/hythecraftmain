package com.hythecraft.hythemain.hook.citizens.traits.dialoguetrait;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.hook.citizens.traits.dialoguetrait.util.DialogueSet;
import com.hythecraft.hythemain.util.NumberUtil;

import net.citizensnpcs.api.persistence.Persist;
import net.citizensnpcs.api.trait.Trait;

public class DialogueTrait extends Trait{
	
	@Persist List<DialogueSet> dialogues; //dialogues when in quest
	@Persist List<String> miscDialogues; //dialogues when no quest dialogue is applicable
	
	public DialogueTrait() {
		super("dialogue");
	}
	
	@EventHandler
	public void click(net.citizensnpcs.api.event.NPCRightClickEvent e){
		if(e.getNPC() != this.getNPC()) return;
		if(dialogues == null) return;
		Player player = e.getClicker();
		
		for(DialogueSet d : dialogues) {
			if(d.isApplicable(player))
			{
				HytheCraft.getInstance().getDialogueManager().startDialogue(d.getDialogueID(), player);
				return;
			}
		}
		
		HytheCraft.getInstance().getDialogueManager().startDialogue(miscDialogues.get(NumberUtil.random(0, miscDialogues.size()-1)), player);
		
	}

	
	
}






























