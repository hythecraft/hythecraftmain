package com.hythecraft.hythemain.core.chat.privatemessage;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.chat.ChatMessageParser;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.Colors;

import net.md_5.bungee.api.chat.TextComponent;

public class PrivateMessageManager {
	
	public void sendMessage(Player from, Player to, String msg) {
		List<TextComponent> send = ChatMessageParser.parseMessage(Colors.parseColors("&bYou &7-> &b" + to.getDisplayName() + " &3➠ &7" + msg), from);
		from.spigot().sendMessage(send.toArray(new TextComponent[send.size()]));
		send = ChatMessageParser.parseMessage(Colors.parseColors("&b" + from.getDisplayName() + " &3➠ &7" + msg), from);
		to.spigot().sendMessage(send.toArray(new TextComponent[send.size()]));
		UserManager.getUser(to).getSelectedProfile().setLastMessage(from.getUniqueId());
	}
	
	public void sendMessage(Player from, Player to, List<TextComponent> msg) {
		List<TextComponent> send = ChatMessageParser.parseMessage(Colors.parseColors("&bYou &7-> &b" + to.getDisplayName() + " &3➠ &7"), from);
		send.addAll(msg);
		from.spigot().sendMessage(send.toArray(new TextComponent[send.size()]));
		
		send = ChatMessageParser.parseMessage(Colors.parseColors("&b" + from.getDisplayName() + " &3➠ &7"), from);
		send.addAll(msg);
		to.spigot().sendMessage(send.toArray(new TextComponent[send.size()]));
		

		UserManager.getUser(to).getSelectedProfile().setLastMessage(from.getUniqueId());
	}
	
	public void replyMessage(Player pl, String msg) {
		ProfileData profile = UserManager.getUser(pl).getSelectedProfile();
		if(profile.getLastMessage() != null)
		{
			sendMessage(pl, Bukkit.getPlayer(profile.getLastMessage()), msg);
		}else
		{
			pl.sendMessage(Colors.parseColors("&7You have no one to reply to"));
		}
	}
}









