package com.hythecraft.hythemain.core.chat.chatmodifier;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.util.Colors;
import com.hythecraft.hythemain.util.JSONUtil;

import net.md_5.bungee.api.chat.TextComponent;

public class JSONItem extends ChatModifierBase{
	final String match = "%item%";
	
	public TextComponent modify(TextComponent toMod, Player player){
		if(player.getInventory().getItemInMainHand().getType() != Material.AIR)
			return new TextComponent(JSONUtil.getItemToJSON(player.getInventory().getItemInMainHand()));
		else
			return new TextComponent(JSONUtil.getJSON(Colors.parseColors("&7" + player.getDisplayName() + "&7's hand"), null, true , Colors.parseColors("&7Ew, Doesnt like it's been washed")));
	}

	@Override
	public boolean applicable(String word) {
		return word.equalsIgnoreCase(match);
	}
	
}







