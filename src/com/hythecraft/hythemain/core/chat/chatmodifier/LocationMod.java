package com.hythecraft.hythemain.core.chat.chatmodifier;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.util.Colors;

import net.md_5.bungee.api.chat.TextComponent;

public class LocationMod extends ChatModifierBase{
	final String match = "%loc%";
	
	public TextComponent modify(TextComponent toMod, Player player){
		Location loc = player.getLocation();
		int x = (int) loc.getX();
		int y = (int) loc.getY();
		int z = (int) loc.getZ();
		return new TextComponent(Colors.parseColors("&a(" + x + "," + y + "," + z + ")&7"));
	}
	
	@Override
	public boolean applicable(String word) {
		return word.equalsIgnoreCase(match);
	}
}
