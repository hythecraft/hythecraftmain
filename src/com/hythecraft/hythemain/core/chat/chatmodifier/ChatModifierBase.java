package com.hythecraft.hythemain.core.chat.chatmodifier;

import org.bukkit.entity.Player;

import net.md_5.bungee.api.chat.TextComponent;

public abstract class ChatModifierBase {
	
	public abstract TextComponent modify(TextComponent toMod, Player player);
	public abstract boolean applicable(String word);
}
