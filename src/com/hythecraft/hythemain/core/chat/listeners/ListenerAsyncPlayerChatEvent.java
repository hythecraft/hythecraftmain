package com.hythecraft.hythemain.core.chat.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.chat.ChatMessageParser;
import com.hythecraft.hythemain.core.chat.util.RecieverChat;
import com.hythecraft.hythemain.core.party.PartyObject;
import com.hythecraft.hythemain.core.user.UserData;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.Colors;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;

public class ListenerAsyncPlayerChatEvent implements Listener{
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void execute(AsyncPlayerChatEvent e) {
		
		e.setCancelled(true);
		UserData user = UserManager.getUser(e.getPlayer());
		RecieverChat reciever = RecieverChat.GLOBAL; 
		reciever = e.getMessage().toLowerCase().contains("@p") ? RecieverChat.PARTY : reciever;
		reciever = e.getMessage().toLowerCase().contains("@") && reciever == RecieverChat.GLOBAL ? RecieverChat.PLAYER : reciever;
		
		List<TextComponent> header = new ArrayList<TextComponent>();
		List<TextComponent> body = ChatMessageParser.parseMessage(e.getMessage(), e.getPlayer());
		
		header.add(new TextComponent(Colors.parseColors(user.getGroup().getGroupPrefix() == null ? "" : user.getGroup().getGroupPrefix() + " &7" + user.getSelectedProfile().getPlayer().getDisplayName() + " ")));
		if(reciever == RecieverChat.PARTY)
			header.add(0, new TextComponent(Colors.parseColors("&d@Party: ")));
		
		List<TextComponent> send = new ArrayList<>();
		send.addAll(header);
		send.addAll(body);

		
		if(reciever == RecieverChat.PLAYER)
		{
			String[] words = e.getMessage().split(" ");
			Player recieving = Bukkit.getPlayer(words[0].replace("@", ""));
			if(recieving != null)
			{
				body.remove(0);
				HytheCraft.getInstance().getPrivetMessageManager().sendMessage(e.getPlayer(), recieving , body);
				return;
			}
			
		}
		
		for(Player p : Bukkit.getOnlinePlayers()) {	
			if(send.isEmpty()) return;
			if(reciever == RecieverChat.PLAYER) return;
			
			if(reciever == RecieverChat.PARTY)
			{
				PartyObject party = UserManager.getUser(p).getSelectedProfile().getCurrentParty();
				PartyObject senderParty = UserManager.getUser(e.getPlayer()).getSelectedProfile().getCurrentParty();
				if(party == null) continue;
				if(senderParty == null) continue;
				if(senderParty != party)continue;
			}

			send.forEach(z -> z.setColor(ChatColor.GRAY));
			if(e.isCancelled())
				p.spigot().sendMessage(send.toArray(new BaseComponent[send.size()]));
		}
		Bukkit.getConsoleSender().sendMessage("CHAT " + e.getPlayer() + ": " + e.getMessage());
		
	}
}
