package com.hythecraft.hythemain.core.chat;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.chat.chatmodifier.ChatModifierBase;
import com.hythecraft.hythemain.core.chat.chatmodifier.JSONItem;
import com.hythecraft.hythemain.core.chat.chatmodifier.LocationMod;
import com.hythecraft.hythemain.util.Colors;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;

public class ChatMessageParser {
	public static String[] replaceForNull = new String[] {"@p", "@P"};
	
	public static List<ChatModifierBase> chatModifiers = new ArrayList<>();
	public static void addMod(ChatModifierBase base) {
		chatModifiers.add(base);
	}
	
	static {
		addMod(new JSONItem());
		addMod(new LocationMod());
	}
	public static List<TextComponent> parseMessage(String msg, Player player){
		String[] words = msg.split(" ");
		List<TextComponent> ret = new ArrayList<>();
		for(String str: words) {
			str = replaceOccurring(str, replaceForNull);
			ret.add(new TextComponent(Colors.parseColors(str + " ")));
		}
		
		
		List<TextComponent> outList = new ArrayList<>();
		for(TextComponent word : ret) 
		{
			for(ChatModifierBase base : chatModifiers) 
			{
				if(base.applicable(word.getText().trim()))
				{
					word = (base.modify(word, player));
					word.addExtra(" ");
				}
			}
			outList.add(word);
		}
		outList.forEach(z -> z.setColor(ChatColor.GRAY));
		return outList;
	}
	
	private static String replaceOccurring(String input, String[] list) {
		for(String s : list)
		{
			input = input.replace(s, "");
		}
		return input;
	}
}
