package com.hythecraft.hythemain.core.trading;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.config.Language;
import com.hythecraft.hythemain.core.inventory.inventories.p2ptrading.PeerToPeerTradeInventory;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.Colors;

public class TradeManager {

	public void requestTrade(Player from, Player to) {
		ProfileData toProfile = UserManager.getUser(to).getSelectedProfile();
		if(toProfile.getLastTradeRequest() != null)
		if(toProfile.getLastTradeRequest().equals(from))
		{
			from.sendMessage(Colors.parseColors(Language.TRADE_REQUEST_ALREADY_SENT));
			return;
		}
		if(UserManager.getUser(from).getSelectedProfile().getLastTradeRequest() != null)
		if(UserManager.getUser(from).getSelectedProfile().getLastTradeRequest().equals(to))
		{
			acceptTrade(from);
			return;
		}
		toProfile.setLastTradeRequest(from);
		
		from.sendMessage(Colors.parseColors("&7Trade request sent."));
		to.sendMessage(Colors.parseColors("&7" + from.getDisplayName() + " &7has sent you a trade request."));
		to.sendMessage(Colors.parseColors("&7Enter &a/trade accept &7to accept"));
		to.sendMessage(Colors.parseColors("&7Enter &c/trade decline &7to decline"));
	}
	
	public void acceptTrade(Player pl) {
		ProfileData profile = UserManager.getUser(pl).getSelectedProfile();
		
		if(profile.getLastTradeRequest() == null)
		{
			pl.sendMessage(Colors.parseColors(Language.NO_TRADE_REQUEST));
			return;
		}
		
		Player p1 = pl;
		Player p2 = profile.getLastTradeRequest();
		
		UserManager.getUser(p1).getSelectedProfile().setLastTradeRequest(null);
		UserManager.getUser(p2).getSelectedProfile().setLastTradeRequest(null);
		
		TradeInstance instance = new TradeInstance(p1,p2);
		new PeerToPeerTradeInventory(instance).openInventory(p1);
	}
	
	public void declineTrade(Player pl) {
		ProfileData profile = UserManager.getUser(pl).getSelectedProfile();
		if(profile.getLastTradeRequest() == null)
		{
			pl.sendMessage(Colors.parseColors(Language.NO_TRADE_REQUEST));
			return;
		}
		profile.getLastTradeRequest().sendMessage(Colors.parseColors("&7" + pl.getDisplayName() + " &cdeclined your trade request."));;
		profile.setLastTradeRequest(null);
	}
}
