package com.hythecraft.hythemain.core.trading;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryCloseEvent.Reason;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.config.Language;
import com.hythecraft.hythemain.core.items.ItemManager;
import com.hythecraft.hythemain.core.items.types.interfaces.UntradableItem;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.Colors;

import lombok.Getter;

public class TradeInstance implements Listener{

	@Getter private Player player1, player2;
	@Getter private List<ItemStack> offer1 = new ArrayList<>(), offer2 = new ArrayList<>();
	@Getter private boolean accept1 = false, accept2 = false;
	
	public TradeInstance(Player player1, Player player2)
	{
		HytheCraft.getInstance().registerListener(this);
		this.player1 = player1;
		this.player2 = player2;
		
		UserManager.getUser(player1).getSelectedProfile().setTradeInstance(this);
		UserManager.getUser(player2).getSelectedProfile().setTradeInstance(this);

		UserManager.getUser(player1).getSelectedProfile().setLastTradeRequest(null);
		UserManager.getUser(player2).getSelectedProfile().setLastTradeRequest(null);
	}
	
	public void acceptTrade(Player player) {
		if(player.equals(player1))
		{
			accept1 = true;
		}
		if(player.equals(player2))
		{
			accept2 = true;
		}
		if(accept1 && accept2) finishTrade();
	}
	
	public boolean addOffer(Player player, ItemStack offer) {
		if(accept1 || accept2) return false;
		if(ItemManager.getCustomItemHandler(ItemManager.getCustomItemType(offer)) instanceof UntradableItem) return false;
		if(player.equals(player1))
		{
			offer1.add(offer);
			return true;
		}
		if(player.equals(player2))
		{
			offer2.add(offer);
			return true;
		}
		return false;
	}
	
	public boolean removeOffer(Player player, ItemStack offer) {
		if(accept1 || accept2) return false;
		if(player.equals(player1))
		{
			if(!offer1.contains(offer)) return false;
			offer1.remove(offer);
			return true;
		}
		if(player.equals(player2))
		{
			if(!offer2.contains(offer)) return false;
			offer2.remove(offer);
			return true;
		}
		return false;
	}

	public void finishTrade() {

		player1.sendMessage(Colors.parseColors(Language.TRADE_ACCEPTED));
		player2.sendMessage(Colors.parseColors(Language.TRADE_ACCEPTED));

		player1.getInventory().addItem(offer2.toArray(new ItemStack[offer2.size()]));
		player2.getInventory().addItem(offer1.toArray(new ItemStack[offer1.size()]));
		
		player1.closeInventory(Reason.OPEN_NEW);
		player2.closeInventory(Reason.OPEN_NEW);

		UserManager.getUser(player1).getSelectedProfile().setTradeInstance(null);
		UserManager.getUser(player2).getSelectedProfile().setTradeInstance(null);
		
		player1 = null;
		player2 = null;
		HytheCraft.getInstance().unregisterListener(this);
	}
	
	public void cancelTrade() {
		player1.sendMessage(Colors.parseColors(Language.TRADE_CANCELLED));
		player2.sendMessage(Colors.parseColors(Language.TRADE_CANCELLED));
		
		player1.getInventory().addItem(offer1.toArray(new ItemStack[offer1.size()]));
		player2.getInventory().addItem(offer2.toArray(new ItemStack[offer2.size()]));
		
		player1.closeInventory(Reason.OPEN_NEW);
		player2.closeInventory(Reason.OPEN_NEW);
		
		UserManager.getUser(player1).getSelectedProfile().setTradeInstance(null);
		UserManager.getUser(player2).getSelectedProfile().setTradeInstance(null);

		player1 = null;
		player2 = null;
		HytheCraft.getInstance().unregisterListener(this);
	}
	
	@EventHandler
	public void closeInvEvent(InventoryCloseEvent e) {
		if(player1 == null || player2 == null) return;
		if(!player1.equals(e.getPlayer()) && !player2.equals(e.getPlayer())) return;
		if(!e.getReason().equals(Reason.OPEN_NEW))
		cancelTrade();
	}
	
	@EventHandler
	public void playerLeaveEvent(PlayerQuitEvent e) {
		if(!player1.equals(e.getPlayer()) && !player2.equals(e.getPlayer())) return;
		cancelTrade();
	}
}






















