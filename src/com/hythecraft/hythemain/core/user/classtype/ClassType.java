package com.hythecraft.hythemain.core.user.classtype;

import java.util.List;

import org.bukkit.Material;

import com.hythecraft.hythemain.util.gui.TextAlignment;

public enum ClassType {
	SWORDSMAN(Material.DIAMOND_SWORD, "Swordsman", "#014"){
		@Override
		public String[] desc() {
			List<String> desc = TextAlignment.truncateText(24, 
					"&7A deadly rogue with strength unmatched to any. The Swordsman uses pure strength and willpower to slash and dismember foes, someone to be at the frontline. The swordsman charges into enemies leaving a trail of destruction anywhere it goes."
					);
			desc.add("");
			desc.add(TextAlignment.centerText(24, "&eLearn more at"));
			desc.add(TextAlignment.centerText(24, "&chythecraft.com/classes"));
			return desc.toArray(new String[desc.size()]);
		}
	},
	RANGER(Material.BOW, "Ranger", "#013"){
		@Override
		public String[] desc() {
			List<String> desc = TextAlignment.truncateText(24, 
					"&7Strength from a distance. The Ranger provides damage from a distance, always one step ahead of you. With a bow and arrow in hand the ranger is always ready to fight enemies and pillage."
					);
			desc.add("");
			desc.add(TextAlignment.centerText(24, "&eLearn more at"));
			desc.add(TextAlignment.centerText(24, "&chythecraft.com/classes"));
			return desc.toArray(new String[desc.size()]);
		}
	},
	ASSASSIN(Material.DIAMOND_HOE, "Assassin", "#015"){
		@Override
		public String[] desc() {
			List<String> desc = TextAlignment.truncateText(24, 
					"&7Danger lurks around every corner. The Assassin lurks in the shadows ready to pounce. Like an eager leopard, the assassin is ready for anything that comes at them."
				);
			desc.add("");
			desc.add(TextAlignment.centerText(24, "&eLearn more at"));
			desc.add(TextAlignment.centerText(24, "&chythecraft.com/classes"));
			return desc.toArray(new String[desc.size()]);
		}
	},
	CASTER(Material.WOODEN_SHOVEL, "Caster", "#016") {;
	@Override
	public String[] desc() {
		List<String> desc = TextAlignment.truncateText(24, 
				"&7A dark force looms over. The caster utilises magic and dark energy to cast a deadly beam of death. The caster needs no blade to inflict wounds on their enemies."
				);
		desc.add("");
		desc.add(TextAlignment.centerText(24, "&eLearn more at"));
		desc.add(TextAlignment.centerText(24, "&chythecraft.com/classes"));
		return desc.toArray(new String[desc.size()]);
	}
	};

	public Material mat;
	public String name;
	public String itemID;
	
	ClassType(Material mat, String name, String itemID)
	{
		this.mat = mat;
		this.name = name;
		this.itemID = itemID;
	}
	
	public static ClassType getTypeByName(String name)
	{
        for (ClassType clazz : values()) { 
        	if(name != null)
            if(clazz.name.contains(name))
            {
            	return clazz;
            }
        } 
        return null;
	}
	
	public String[] desc() {
		return null;
	}
	
} 