package com.hythecraft.hythemain.core.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.items.ItemManager;
import com.hythecraft.hythemain.core.items.ModifierManager;
import com.hythecraft.hythemain.core.items.modifiers.StaticModifier;
import com.hythecraft.hythemain.core.listener.events.profile.ProfileLevelUpEvent;
import com.hythecraft.hythemain.core.listener.events.updates.UpdateSecondEvent;
import com.hythecraft.hythemain.core.party.PartyObject;
import com.hythecraft.hythemain.core.pet.util.PetType;
import com.hythecraft.hythemain.core.trading.TradeInstance;
import com.hythecraft.hythemain.core.user.classtype.ClassType;
import com.hythecraft.hythemain.persistence.serialization.ClassTypeSerializer;
import com.hythecraft.hythemain.persistence.serialization.ItemStackSerializer;
import com.hythecraft.hythemain.persistence.serialization.LocationSerializer;
import com.hythecraft.hythemain.persistence.serialization.PetTypeSerializer;
import com.hythecraft.hythemain.util.Colors;
import com.hythecraft.hythemain.util.NumberUtil;
import com.hythecraft.hythemain.util.annotations.AlternateSerializable;
import com.hythecraft.hythemain.util.annotations.DoNotSerialize;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class ProfileData implements Listener{
	
	@Getter @DoNotSerialize private Player player;
	@Getter @DoNotSerialize private UserData user;
	
	@Getter private String profileID;
	@Getter @AlternateSerializable(ClassTypeSerializer.class) private ClassType classType;
	
	//Attributes
	@Getter @Setter private int maxHealth;
	@Getter @Setter private int maxEnergy;
	
	@Getter @Setter private int health;
	@Getter @Setter private int energy;
	
	@Getter @Setter private int level;
	@Getter @Setter private int experience;
	
	@Getter @Setter private int defense;
	@Getter @Setter private float speed;
	
	@Getter @Setter private int money;
	
	//Misc
	
	@Getter @Setter private @AlternateSerializable(LocationSerializer.class) Location loc;
	@Getter @Setter private String lastTown;
	
	@Getter @Setter private Map<String, Integer> quests;
	
	//Preferences
	@Getter @Setter private boolean acceptPartyInvites = true;
	@Getter @Setter private boolean acceptTradeInvites = true;
	
	@Getter @Setter private List<String> charactersFound;
	
	@Getter @AlternateSerializable(ItemStackSerializer.class) private ItemStack[] playerInventory;
	@Getter @AlternateSerializable(ItemStackSerializer.class) private ItemStack[] playerArmor;
	@Getter @Setter @AlternateSerializable(PetTypeSerializer.class) private PetType pet;
	
	@Getter @Setter @DoNotSerialize private UUID lastMessage;
	@Getter @Setter @DoNotSerialize private PartyObject lastPartyRequest;
	@Getter @Setter @DoNotSerialize private PartyObject currentParty;
	
	@Getter @Setter @DoNotSerialize private TradeInstance tradeInstance;
	@Getter @Setter @DoNotSerialize private Player lastTradeRequest;
	
	//Called on profile creation
	public ProfileData(ClassType type) {
		this.classType = type;
		this.profileID = UUID.randomUUID().toString();
		//Apply defaults
		maxHealth = 100;
		health = maxHealth;
		maxEnergy = 20;
		energy = maxEnergy;
		quests = new HashMap<>();
		charactersFound = new ArrayList<>();
		level = 1;
		loc = HytheCraft.getSpawnLoc();
	}
	
	//Called each time a player chooses the profile
	public void init(Player player) {
		HytheCraft.getInstance().registerListener(this);
		this.player = player;
		this.user = UserManager.getUser(player);
		player.teleport(loc == null ? HytheCraft.getSpawnLoc() : loc);
		player.removePotionEffect(PotionEffectType.BLINDNESS);
		player.setGameMode(GameMode.SURVIVAL);
		
		player.getInventory().setArmorContents(playerArmor == null ? new ItemStack[0] : playerArmor);
		player.getInventory().setContents(playerInventory == null ? new ItemStack[0] : playerInventory);
		calculateStats();
	}
	
	public void saveProfile() {
		playerInventory = player.getInventory().getContents() == null ? new ItemStack[0] : player.getInventory().getContents();
		playerArmor = player.getInventory().getArmorContents() == null ? new ItemStack[0] : player.getInventory().getArmorContents();

		HytheCraft.getInstance().getMongoHook().saveData(profileID.toString(), this, "profiles");
	}

	public void calculateStats()
	{
		setDefaultStats();
		ItemStack hand = player.getInventory().getItemInMainHand();
		ItemStack[] armor = player.getInventory().getArmorContents();
		
		List<ItemStack> items = new ArrayList<>();
		items.add(hand);
		items.addAll(Arrays.asList(armor));
		
		for(ItemStack item : items) {
			if(item == null || item.getType() == Material.AIR) continue;
			if(ItemManager.getCustomItemType(item) == null) continue;
			if(!ItemManager.getCustomItemHandler(ItemManager.getCustomItemType(item)).canUse(player)) continue;
			
			for(StaticModifier mod : ModifierManager.getStaticMods(item))
			{
				mod.applyMod(this);
			}
		}
		
	}
	
	private void setDefaultStats() {
		//DO NOT CHANGE HEALTH & ENERGY YET
		maxHealth = 100 + level * 5;
		maxEnergy = 20;
		
		defense = 0;
		speed = 0.2f;
	}
	
	@EventHandler
	public void execute(UpdateSecondEvent e) {
		//LEVEL CHECK
		if(this.experience > getRequiredExp(this.level))
		{
			this.experience -= getRequiredExp(this.level);
			this.level++;
			Bukkit.getPluginManager().callEvent(new ProfileLevelUpEvent(player, this));
		}
		
		this.health = (int) NumberUtil.clamp(this.health, 0, this.maxHealth);
		this.energy = (int) NumberUtil.clamp(this.energy, 0, this.maxEnergy);
		
		this.player.setWalkSpeed(NumberUtil.clamp(speed, 0, 1));
		this.player.setLevel(level);
		this.player.setExp(NumberUtil.clamp((float) (experience /(25 * level * ( 1 + level) )), (float)0.0, (float)1.0));
		this.player.setHealth(NumberUtil.clamp(Math.round((float)((float)health/(float)maxHealth)*20),1 , 20));
	
		HytheCraft.getInstance().getTablistManager().update(player);
		
		setDisplayPrefix();
	}
	
	private void setDisplayPrefix() {
		try {
        Scoreboard scoreboard = HytheCraft.getInstance().getServer().getScoreboardManager().getMainScoreboard(); 
        Team team = scoreboard.getTeam(player.getName());
		Objective objective = scoreboard.getObjective("showhealth");
		
        if(team == null)
        	team = scoreboard.registerNewTeam(player.getName());
		if(objective == null)
			objective = scoreboard.registerNewObjective("showhealth", "dummy", "");
        
        team.setPrefix(Colors.parseColors((user.getGroup().getGroupPrefix() != null ? user.getGroup().getGroupPrefix() + " ": "") + "&7(&aLvl." + level + "&7) "));
    	team.addEntry(player.getName());

		objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
		objective.setDisplayName(Colors.parseColors("&c❤"));
		objective.getScore(player.getName()).setScore(health);	
		}catch(Exception e) {}
	}
	
	private static final int getRequiredExp(int level) {
		return 25 * level * ( 1 + level);
	}
}




















