package com.hythecraft.hythemain.core.user;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.listener.events.updates.UpdateSecondEvent;
import com.hythecraft.hythemain.core.permission.PermissionGroup;
import com.hythecraft.hythemain.core.user.classtype.ClassType;
import com.hythecraft.hythemain.persistence.serialization.PermissionGroupSerializer;
import com.hythecraft.hythemain.util.Colors;
import com.hythecraft.hythemain.util.annotations.AlternateSerializable;
import com.hythecraft.hythemain.util.annotations.DoNotSerialize;
import com.hythecraft.hythemain.util.gui.TextAlignment;
import com.hythecraft.hythemain.util.location.LocationUtil;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class UserData implements Listener{
	
	private final static int saveDelay = 120;
	
	@Getter String playerUUID;
	@Getter List<String> profiles;
	@Getter @Setter @AlternateSerializable(PermissionGroupSerializer.class) PermissionGroup group;
	
	@Getter @Setter @DoNotSerialize ProfileData selectedProfile;
	@Getter @Setter @DoNotSerialize String lastLinkReq;
	public void saveUser() {
		HytheCraft.getInstance().getMongoHook().saveData(playerUUID, this, "players");
	}
	
	public void loadProfile(String uuid) {
		selectedProfile = HytheCraft.getInstance().getMongoHook().getObject(uuid.toString(), ProfileData.class, "profiles");
		selectedProfile.init(Bukkit.getPlayer(UUID.fromString(playerUUID)));
	}
	
	public void removeProfile(String uuid) {
		profiles.remove(uuid);
		saveUser();
	}
	
	public void newProfile(ClassType type) {
		ProfileData profile = new ProfileData(type);	
		addProfile(profile.getProfileID());
		selectedProfile = profile;
		selectedProfile.init(Bukkit.getPlayer(UUID.fromString(playerUUID)));
		selectedProfile.saveProfile();
	}
	
	public void addProfile(String uuid) {
		profiles = profiles == null ? new ArrayList<>() : profiles;
		
		profiles.add(uuid);
		saveUser();
	}


	@SuppressWarnings("deprecation")
	@EventHandler
	public void execute(UpdateSecondEvent e) {
		
		if(selectedProfile == null) return;
		ProfileData p = selectedProfile;
		Location loc = p.getPlayer().getLocation();
		String hotbarMessage = TextAlignment.centerText(64, 
				"&c" + p.getHealth() + "/" + p.getMaxHealth(), 
				"&f" + (int)loc.getX() + " " + LocationUtil.directionAsChar(p.getPlayer()).getCh() + " &f" + (int)loc.getZ() , 
				"&b" + p.getEnergy() + "/" + p.getMaxEnergy());
		p.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(Colors.parseColors(hotbarMessage)));
	}

	@DoNotSerialize private int lastSave = 0;
	@SuppressWarnings("deprecation")
	@EventHandler
	public void save(UpdateSecondEvent e) {
		lastSave++;
		if(lastSave != saveDelay) return;
		lastSave = 0;
		
		saveUser();
		if(selectedProfile == null) return;
		selectedProfile.saveProfile();
		selectedProfile.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(Colors.parseColors("&7Saved Progress!")));
	}
	
	public String getOnlineUUID() {
		try {

		    Pattern PATTERN = Pattern.compile("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})");
			String url = "https://api.mojang.com/users/profiles/minecraft/"+Bukkit.getPlayer(UUID.fromString(playerUUID)).getName();
			@SuppressWarnings("deprecation")
			String UUIDJson = IOUtils.toString(new URL(url));
			
			JSONObject UUIDObject = (JSONObject) JSONValue.parseWithException(UUIDJson);
			return PATTERN.matcher(UUIDObject.get("id").toString()).replaceFirst("$1-$2-$3-$4-$5");
			
		
		}catch(Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
}
















