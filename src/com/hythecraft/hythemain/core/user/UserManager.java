package com.hythecraft.hythemain.core.user;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.inventory.inventories.profilepicker.ProfilePickerInventory;
import com.hythecraft.hythemain.util.Colors;

public class UserManager {

	static Map<UUID, UserData> cachedData = new HashMap<>();
	
	public static UserData getUser(Player player) {
		UUID uuid = player.getUniqueId();
		
		if(cachedData.containsKey(uuid))
			return cachedData.get(uuid);
		else
		{
			UserData newUser = HytheCraft.getInstance().getMongoHook().getObject(uuid.toString(), UserData.class, "players");
			HytheCraft.getInstance().registerListener(newUser);
			cachedData.put(uuid, newUser);
			return getUser(player);
		}
	}
	
	public static ProfileData getProfile(Player player) {
		return getUser(player).getSelectedProfile();
	}
	
	@SuppressWarnings("deprecation")
	public static void loadResourcePack(Player player) {
		player.sendMessage(Colors.parseColors("&7Loading Resource Pack..."));
		//player.setResourcePack("https://www.dropbox.com/s/5otd5nrcllpv5ox/Excalibur_V1.4.zip?dl=1");
		player.setResourcePack("https://www.dropbox.com/s/156j53sqdlpuk3i/Excalibur_V1.15.zip?dl=1");
	}
	
	public static void characterSelectionMode(Player player) {
		if(player.getOpenInventory().getType() != InventoryType.CHEST) {
			player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 99999999, 999999999));
			player.setGameMode(GameMode.SPECTATOR);
			player.teleport(new Location(player.getWorld(), 1000, 300 , 1000));
			new ProfilePickerInventory(player).openInventory(player);
		}
	}
	
	public static void onDisable() {
		for(Player player: Bukkit.getOnlinePlayers())
		{
			if(getUser(player).getSelectedProfile() != null)
				getUser(player).getSelectedProfile().saveProfile();
			getUser(player).saveUser();
		}
	}
}
