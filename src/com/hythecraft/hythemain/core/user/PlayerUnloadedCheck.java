package com.hythecraft.hythemain.core.user;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.hythecraft.hythemain.core.listener.events.updates.UpdateSecondEvent;

public class PlayerUnloadedCheck implements Listener
{
	@EventHandler
	public void execute(UpdateSecondEvent e) {
		for(Player player : Bukkit.getOnlinePlayers()){
			if(UserManager.getUser(player).selectedProfile != null) continue;
			UserManager.characterSelectionMode(player);
		}
	}

}
