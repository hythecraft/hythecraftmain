package com.hythecraft.hythemain.core.party.util;

public enum PartyRemoveReason {
	LEAVE,
	KICK
}
