package com.hythecraft.hythemain.core.party;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.hythecraft.hythemain.config.Language;
import com.hythecraft.hythemain.core.party.util.PartyRemoveReason;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.Colors;

import lombok.Getter;

public class PartyObject implements Listener{
	public final static int maxPartySize = 4; //INCLUDING LEADER
	
	@Getter private UUID partyID;
	
	@Getter private UUID[] players = new UUID[maxPartySize-1];
	@Getter private UUID leader;
	
	public PartyObject(Player leader) {
		this.partyID = UUID.randomUUID();
		this.leader = leader.getUniqueId();
	}
	
	public boolean addPlayer(Player player) {
		if(getPlayerCount() == maxPartySize) return false;
		if(getAllPlayers().contains(player.getUniqueId())) return false;
		for(int i = 0; i < players.length; i++) {
			if(players[i] == null)
			{
				players[i] = player.getUniqueId();
				UserManager.getUser(player).getSelectedProfile().setCurrentParty(this);
				messageAll(Colors.parseColors(Language.PARTY_PREFIX + player.getDisplayName() + Language.PARTY_PLAYER_JOIN));
				return true;
			}
		}
		return false;
	}
	
	public void removePlayer(Player player, PartyRemoveReason reason) {
		for(int i = 0; i < players.length; i++) {
			if(players[i] == null) continue;
			if(players[i].equals(player.getUniqueId()))
			{
				players[i] = null;
			}
		}
		if(reason == PartyRemoveReason.KICK){
			messageAll(Colors.parseColors(Language.PARTY_PREFIX + "&a" + player.getDisplayName() + Language.PARTY_KICKED_PARTY));
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.PARTY_KICKED_YOU));
		}
		else if(reason == PartyRemoveReason.LEAVE) {
			messageAll(Colors.parseColors(Language.PARTY_PREFIX + "&a" + player.getDisplayName() + Language.PARTY_PLAYER_LEFT));
		}
		UserManager.getUser(player).getSelectedProfile().setCurrentParty(null);	
	}
	
	public void disband() {
		messageAll(Colors.parseColors(Language.PARTY_PREFIX + Language.PARTY_DISBANDED));
		UserManager.getUser(Bukkit.getPlayer(leader)).getSelectedProfile().setCurrentParty(null);
		for(UUID player : players) {
			if(player == null) continue;
			UserManager.getUser(Bukkit.getPlayer(player)).getSelectedProfile().setCurrentParty(null);
		}
		leader = null;
		players = new UUID[maxPartySize-1];
	}
	
	public int getPlayerCount() {
		int count = 0;
		for(UUID uuid : getAllPlayers())
		{
			if(uuid == null) continue;
			count++;
		}
		return count;
	}
	
	public List<UUID> getAllPlayers() {
		List<UUID> list = new ArrayList<>();
		list.add(leader);
		list.addAll(Arrays.asList(players));
		return list;
	}
	
	public void messageAll(final String message) {
		for(UUID uuid : getAllPlayers()) {
			if(uuid == null) continue;
			Bukkit.getPlayer(uuid).sendMessage(message);
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		if(getAllPlayers().contains(e.getPlayer().getUniqueId()))
		{
			removePlayer(e.getPlayer(), PartyRemoveReason.LEAVE);
		}
	}
	
	public void invitePlayer(Player player) {
		if(!UserManager.getUser(player).getSelectedProfile().isAcceptPartyInvites())
		{
			messageAll(Colors.parseColors("&a" + player.getDisplayName() + " &7has party invited disabled."));
			return;
		}
		
		messageAll(Colors.parseColors("&a" + player.getDisplayName() + " &7has been invited to the party."));
		player.sendMessage(Colors.parseColors("&7You have been invited to &a" +Bukkit.getPlayer(leader).getDisplayName() + "'s &7Party"));
		player.sendMessage(Colors.parseColors("&7Enter &a/party accept &7to accept"));
		player.sendMessage(Colors.parseColors("&7Enter &c/party decline &7to decline"));
		UserManager.getUser(player).getSelectedProfile().setLastPartyRequest(this);
	}
	
}


















