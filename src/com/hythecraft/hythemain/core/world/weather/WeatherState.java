package com.hythecraft.hythemain.core.world.weather;

public enum WeatherState {
	SUNNY,
	CLEAR,
	RAIN,
	STORM
}
