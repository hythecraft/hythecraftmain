package com.hythecraft.hythemain.core.world.weather;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.listener.events.updates.UpdateTickEvent;
import com.hythecraft.hythemain.util.NumberUtil;

import lombok.Getter;

public class WeatherManager implements Listener{
	
	@Getter private float windDirection;
	@Getter private float windSpeed;
	@Getter private WeatherState weatherState;
	
	public WeatherManager() {
		HytheCraft.getInstance().registerListener(this);
		windDirection = NumberUtil.random(0, 360);
		windSpeed = NumberUtil.random(0f, 1f);
	}

	@EventHandler
	public void execute(UpdateTickEvent e) {
		windDirection += NumberUtil.random(-1, 1);
		windSpeed += NumberUtil.random(-.05f, .05f);
	}
}
