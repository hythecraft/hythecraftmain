package com.hythecraft.hythemain.core.inventory.inventories.misc;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.inventory.util.model.InventoryBase;
import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.ItemManager;
import com.hythecraft.hythemain.core.items.impl.misc.Satchel;
import com.hythecraft.hythemain.core.items.types.interfaces.UntradableItem;
import com.hythecraft.hythemain.persistence.serialization.ItemStackSerializer;

import de.tr7zw.changeme.nbtapi.NBTItem;

public class SatchelInventory extends InventoryBase{

	protected String satchelID;
	protected Player player;
	
	public SatchelInventory(Player player, ItemStack[] items, String satchelID) {
		super(player, "SATCHEL", "&7Satchel", 27);
		int index = 0;
		for(ItemStack item : items) {
			if(item == null) {index++; continue;}
				setItem(item, index);
			index++;
		}
		this.satchelID = satchelID;
		this.player = player;
		this.defaultClickHandler = e -> {
			CustomItemType type = ItemManager.getCustomItemType(e.getItem());
			if(type == null || type == CustomItemType.SATCHEL) return;
			if(ItemManager.getCustomItemHandler(type) instanceof UntradableItem) return;
			e.getEventRaw().setCancelled(false);		
		};
		
	}
	
	@Override
	@EventHandler
	public void inventoryCloseEvent(InventoryCloseEvent e) {
		if(!e.getPlayer().getUniqueId().equals(player.getUniqueId())) return;
		ItemStack[] items = e.getInventory().getContents();
		ItemStack satchel = e.getPlayer().getInventory().getItemInMainHand();
		NBTItem satchelNBT = new NBTItem(satchel);
		
		satchelNBT.setString(Satchel.CONTENT_TAG, new ItemStackSerializer().serialize(items));
		e.getPlayer().getInventory().setItemInMainHand(satchelNBT.getItem());
		HytheCraft.getInstance().unregisterListener(this);
	}

}














