package com.hythecraft.hythemain.core.inventory.inventories.quest;

import org.bukkit.Material;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.tuple.Pair;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.inventory.util.model.PagedInventory;
import com.hythecraft.hythemain.core.quests.util.QuestBase;
import com.hythecraft.hythemain.util.ItemBuilder;

public class QuestInventory extends PagedInventory{

	public QuestInventory(Player player) {
		super(player, "QUEST", "&6Quests", 54);
		setStaticItem(new ItemBuilder(Material.LIGHT_GRAY_STAINED_GLASS_PANE).name(" ").create(),1,2,3,4,5,6,7);
		for(QuestBase quest : HytheCraft.getInstance().getQuestManager().getAvailableQuests(player))
		{
			addItem(new ItemBuilder(Material.WRITABLE_BOOK).coloredName(quest.getName()).coloredLore(quest.getQuestDescription(), "&6(Meet Buby at the Cave)").create());
		}
		
	}

	@Override
	protected Pair<Integer, ItemStack> nextPageButton() {
		return Pair.of(8, new ItemBuilder(Material.ARROW).coloredName("&aNext Page").create());
	}

	@Override
	protected Pair<Integer, ItemStack> previousPageButton() {
		return Pair.of(0, new ItemBuilder(Material.ARROW).coloredName("&aPrevious Page").create());
	}

	@Override
	protected ItemStack emptyPageButton() {
		return new ItemBuilder(Material.LIGHT_GRAY_STAINED_GLASS_PANE).name(" ").create();
	}
}
