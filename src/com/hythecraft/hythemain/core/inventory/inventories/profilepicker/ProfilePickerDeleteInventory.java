package com.hythecraft.hythemain.core.inventory.inventories.profilepicker;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.inventory.util.model.InventoryBase;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.ItemBuilder;
import com.hythecraft.hythemain.util.gui.ItemRepo;

public class ProfilePickerDeleteInventory extends InventoryBase{

	public ProfilePickerDeleteInventory(Player player, String uuid) {
		super(player, "PROFILE_PICKER_DELETE", "&b&oAre you sure?", 27);
		setItem(new ItemBuilder(ItemRepo.TICK).coloredName("&aConfirm").coloredLore("&7This will delete your Character FOREVER", "&7You will not be able to recover this character").create(), e -> {UserManager.getUser(e.getPlayer()).removeProfile(uuid); new ProfilePickerInventory(e.getPlayer()).openInventory(e.getPlayer());}, 11);
		setItem(new ItemBuilder(ItemRepo.CROSS).coloredName("&cCancel").create(), e -> {new ProfilePickerInventory(player).openInventory(player);}, 15);
	}
}
