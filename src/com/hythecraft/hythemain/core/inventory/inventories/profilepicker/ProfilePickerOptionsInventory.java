package com.hythecraft.hythemain.core.inventory.inventories.profilepicker;

import java.util.EnumSet;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.inventory.util.model.InventoryBase;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.core.user.classtype.ClassType;
import com.hythecraft.hythemain.util.ItemBuilder;
import com.hythecraft.hythemain.util.gui.ItemRepo;

public class ProfilePickerOptionsInventory extends InventoryBase{

	public ProfilePickerOptionsInventory(Player player) {
		super(player, "PROFILE_PICKER_OPTIONS", "&bChoose a class", 27);
		setItem(new ItemBuilder(ItemRepo.BACK).coloredName("&c&lBack").create(), e -> {new ProfilePickerInventory(e.getPlayer()).openInventory(player);}, 8);
		EnumSet.allOf(ClassType.class)
        .forEach(clazz -> addItem(new ItemBuilder(clazz.mat).coloredName(clazz.name).coloredLore(clazz.desc()).create(), z -> {UserManager.getUser(z.getPlayer()).newProfile(clazz); z.getPlayer().closeInventory();}));
	}

}
