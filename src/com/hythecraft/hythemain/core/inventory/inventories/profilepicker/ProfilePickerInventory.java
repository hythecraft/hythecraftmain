package com.hythecraft.hythemain.core.inventory.inventories.profilepicker;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.inventory.util.model.InventoryBase;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.Colors;
import com.hythecraft.hythemain.util.ItemBuilder;
import com.hythecraft.hythemain.util.gui.ItemRepo;

public class ProfilePickerInventory extends InventoryBase{

	public ProfilePickerInventory(Player player) {
		super(player, "PROFILE_PICKER", "&bCharacter Selection", 27);
		setItem(new ItemBuilder(ItemRepo.PLUS).coloredName("&aClick to create a new Character").create(), e -> {new ProfilePickerOptionsInventory(player).openInventory(player);} , 1, 2, 3, 4, 5);
		setItem(new ItemBuilder(ItemRepo.BACK).coloredName("&cLogout").create(), e -> {player.kickPlayer(Colors.parseColors("&c&lLogged Out\n&7Thankyou for playing HytheCraft"));} , 22);
		if(UserManager.getUser(player).getProfiles() == null) return;
		
		int profileIndex = 1;
		for(String uuid : UserManager.getUser(player).getProfiles())
		{
			try {
				ProfileData profile = HytheCraft.getInstance().getMongoHook().getObject(uuid, ProfileData.class, "profiles");
				setItem(new ItemBuilder(profile.getClassType().mat).coloredName(profile.getClassType().name).coloredLore("&7Level: " + profile.getLevel(), " ", "&7Right click to delete character").create(), 
						l -> {UserManager.getUser(l.getPlayer()).loadProfile(profile.getProfileID());},
						r -> {new ProfilePickerDeleteInventory(r.getPlayer(), profile.getProfileID()).openInventory(r.getPlayer());},
						profileIndex);
				profileIndex++;
			}
			catch(Exception e) {}
			}	
	}

}
