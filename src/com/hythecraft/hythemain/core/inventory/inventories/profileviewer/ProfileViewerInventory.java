package com.hythecraft.hythemain.core.inventory.inventories.profileviewer;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.inventory.util.model.InventoryBase;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.ItemBuilder;
import com.hythecraft.hythemain.util.gui.ItemRepo;


public class ProfileViewerInventory extends InventoryBase{

	public ProfileViewerInventory(Player player) {
		super(player, "PROFILE_VIEW", player.getDisplayName() + "'s &7Profile", 54);
		ItemStack[] armor = player.getInventory().getArmorContents();
		ProfileData profile = UserManager.getUser(player).getSelectedProfile();
		
		if(profile == null) return;
		
		for(int i = 0; i < 4; i++) {
			if(armor[i] == null) continue;
			setItem(armor[i], (4-i) * 9);
		}
		
		setItem(new ItemBuilder(player).coloredName(player.getDisplayName()).coloredLore("&7Level: &f" + profile.getLevel()).create(), 4);
		
		if(player.getInventory().getItemInMainHand() != null && player.getInventory().getItemInMainHand().getType() != Material.AIR)
			setItem(player.getInventory().getItemInMainHand(), 19);
		
		if(profile.isAcceptTradeInvites())
			setItem(new ItemBuilder(ItemRepo.TRADE.mat).coloredName("&a&lTrade").coloredLore("&7Request to trade with player").create(), e -> {HytheCraft.getInstance().getTradeManager().requestTrade(e.getPlayer(), player);}, 53);
		else
			setItem(new ItemBuilder(ItemRepo.TRADE.mat).coloredName("&7&lTrade").coloredLore("&7Player has trades disabled").create(), 53);
	}
}
