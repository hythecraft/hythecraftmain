package com.hythecraft.hythemain.core.inventory.inventories.p2ptrading;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;

import com.hythecraft.hythemain.core.inventory.util.model.SharedAsyncInventory;
import com.hythecraft.hythemain.core.trading.TradeInstance;
import com.hythecraft.hythemain.util.ItemBuilder;
import com.hythecraft.hythemain.util.gui.ItemRepo;

public class PeerToPeerTradeInventory extends SharedAsyncInventory{

	public static final int[] box1 = new int[] { 0, 1, 2, 3,
            9,10,11,12,
           18,19,20,21,
           27,28,29,30,
           36,37,38,39,
           45,46,47,48};

	public static final int[] box2 = new int[] { 5, 6, 7, 8,
           14,15,16,17,
           23,24,25,26,
           32,33,34,35,
           41,42,43,44,
           50,51,52,53};
	
	public PeerToPeerTradeInventory(TradeInstance instance) {
		super("P2P_TRADE", "&7Trade inventory", 54, instance.getPlayer1(), instance.getPlayer2());
		setItems(instance);
		
		this.defaultClickHandler = l ->
		{
			InventoryClickEvent e = l.getEventRaw();
			
			if(instance.isAccept1() || instance.isAccept2()) return;
			
			if(e.getClickedInventory().getType() != InventoryType.CHEST) {
				//CLICKED INVENTORY
				instance.addOffer((Player)e.getWhoClicked(), e.getCurrentItem());
				e.getWhoClicked().getInventory().remove(e.getCurrentItem());
			
				setItems(instance);
			}
			else
			{
				//CLICKED TRADE WINDOW
				if(instance.removeOffer((Player)e.getWhoClicked(), e.getCurrentItem()))
					e.getWhoClicked().getInventory().addItem(e.getCurrentItem());
				setItems(instance);
			}
			this.updateHandlers();
		};
	}
	
	public void setItems(TradeInstance instance) {
		this.setItemAddList(new ArrayList<>());
		this.setItemList(new HashMap<>());
		setItem(new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).name(" ").create(), 4,13,22,31,40,49);
		setItem(new ItemBuilder(ItemRepo.TICK.mat).coloredName("&aConfirm Trade").coloredLore("&7Locks trade").create(), l -> {instance.acceptTrade(l.getPlayer());}, 22);
		setItem(new ItemBuilder(ItemRepo.CROSS.mat).coloredName("&cDecline trade").create(), l -> {instance.acceptTrade(l.getPlayer());}, 31);
		for(int o1 = 0; o1 < instance.getOffer1().size(); o1++) {
			setItem(instance.getOffer1().get(o1), box1[o1]);
		}
		
		for(int o2 = 0; o2 < instance.getOffer2().size(); o2++) {
			setItem(instance.getOffer2().get(o2), box2[o2]);
		}
		this.updateHandlers();
	}
	

}


















