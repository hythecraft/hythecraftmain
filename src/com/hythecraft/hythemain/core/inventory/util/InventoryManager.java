package com.hythecraft.hythemain.core.inventory.util;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.inventory.util.model.InventoryBase;

import de.tr7zw.changeme.nbtapi.NBTItem;
import lombok.Getter;

public class InventoryManager implements Listener{
	@Getter
	public HashMap<String, InventoryBase> inventoryCache = new HashMap<>();

	public InventoryManager() {
		HytheCraft.getInstance().registerListener(this);
	}
	
	public void registerInstance(InventoryBase base, String id) {
		inventoryCache.put(id, base);
	}
	
	public void unregisterInstance(String id)
	{
		if(inventoryCache.containsKey(id))
			inventoryCache.remove(id);
	}

	@EventHandler
	public void dragHandler(InventoryDragEvent e) {
		if(e.getInventory().getType() == InventoryType.PLAYER) return;
		e.setCancelled(true);
	}
	
	@EventHandler
	public void clickHandler(InventoryClickEvent e) {
		if(e.getClick() == ClickType.SHIFT_LEFT || e.getClick() == ClickType.SHIFT_RIGHT) e.setCancelled(true);
		if(e.getClickedInventory().getType() == InventoryType.PLAYER) return;
		e.setCancelled(true);

		if(e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) return;
		NBTItem item = new NBTItem(e.getCurrentItem());
		String uuid = item.getString(InventoryBase.RANDOM_UUID);
		if(uuid.isEmpty()) return;
		if(!inventoryCache.containsKey(uuid)) return;
		inventoryCache.get(uuid).playerInventoryClickEvent(e);
	}
	
	@EventHandler
	public void playerDropItemEvent(PlayerDropItemEvent e)
	{
		if(e.getItemDrop().getItemStack() == null || e.getItemDrop().getItemStack().getType() == Material.AIR) return;
		NBTItem item = new NBTItem(e.getItemDrop().getItemStack());
		String uuid = item.getString(InventoryBase.RANDOM_UUID);
		if(uuid.isEmpty()) return;
		if(!inventoryCache.containsKey(uuid)) return;
		
		inventoryCache.get(uuid).playerDropItemEvent(e);
	}

	
}
