package com.hythecraft.hythemain.core.inventory.util;

public interface InventoryClickHandler {
	void exec(InventoryClickContext c);
}
