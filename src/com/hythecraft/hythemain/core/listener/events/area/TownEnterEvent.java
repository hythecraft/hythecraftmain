package com.hythecraft.hythemain.core.listener.events.area;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.hythecraft.hythemain.core.area.town.util.TownBase;

import lombok.Getter;

public class TownEnterEvent extends Event{

	private static final HandlerList handlers = new HandlerList();
	

	@Getter private Player player;
	@Getter private TownBase town;
	
	public TownEnterEvent(Player player, TownBase town) {
		this.player = player;
		this.town = town;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
}
