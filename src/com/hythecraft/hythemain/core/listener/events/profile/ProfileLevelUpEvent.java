package com.hythecraft.hythemain.core.listener.events.profile;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.hythecraft.hythemain.core.user.ProfileData;

import lombok.Getter;

public class ProfileLevelUpEvent extends Event{
	private static final HandlerList handlers = new HandlerList();
	

	@Getter private Player player;
	@Getter private ProfileData profile;
	
	public ProfileLevelUpEvent(Player player, ProfileData profile) {
		this.player = player;
		this.profile = profile;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
}