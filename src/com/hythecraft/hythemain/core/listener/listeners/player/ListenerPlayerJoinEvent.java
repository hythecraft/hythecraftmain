package com.hythecraft.hythemain.core.listener.listeners.player;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.hythecraft.hythemain.core.user.UserManager;

public class ListenerPlayerJoinEvent implements Listener
{
	@EventHandler
	public void execute(PlayerJoinEvent e) {	
		e.getPlayer().getInventory().clear();
		UserManager.loadResourcePack(e.getPlayer());
		UserManager.characterSelectionMode(e.getPlayer());
	}
}
