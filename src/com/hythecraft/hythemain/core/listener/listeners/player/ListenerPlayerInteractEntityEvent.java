package com.hythecraft.hythemain.core.listener.listeners.player;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import com.hythecraft.hythemain.core.inventory.inventories.profileviewer.ProfileViewerInventory;

public class ListenerPlayerInteractEntityEvent implements Listener
{
	@EventHandler
	public void execute(PlayerInteractEntityEvent e) {	
		Entity ent = e.getRightClicked();
		Player player = e.getPlayer();
		
		if(!(ent instanceof Player)) return;
		if(!player.isSneaking()) return;
		
		new ProfileViewerInventory((Player)ent).openInventory(player);
	}
}
