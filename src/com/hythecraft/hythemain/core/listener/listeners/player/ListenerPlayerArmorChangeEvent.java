package com.hythecraft.hythemain.core.listener.listeners.player;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.destroystokyo.paper.event.player.PlayerArmorChangeEvent;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;

public class ListenerPlayerArmorChangeEvent implements Listener{

	@EventHandler
	public void execute(PlayerArmorChangeEvent e) {
		ProfileData profile = UserManager.getUser(e.getPlayer()).getSelectedProfile();
		if(profile == null) return;
		profile.calculateStats();
	}
}
