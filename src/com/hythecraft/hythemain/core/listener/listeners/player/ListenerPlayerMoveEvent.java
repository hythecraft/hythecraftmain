package com.hythecraft.hythemain.core.listener.listeners.player;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.hythecraft.hythemain.core.user.UserManager;

public class ListenerPlayerMoveEvent implements Listener{
	@EventHandler
	public void execute(PlayerMoveEvent e) {
		if(UserManager.getUser(e.getPlayer()).getSelectedProfile() == null) return;
		UserManager.getUser(e.getPlayer()).getSelectedProfile().setLoc(e.getPlayer().getLocation());
		
	}
}
