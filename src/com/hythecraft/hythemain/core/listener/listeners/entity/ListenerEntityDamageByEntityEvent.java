package com.hythecraft.hythemain.core.listener.listeners.entity;

import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import com.hythecraft.hythemain.core.combat.AttackInstance;
import com.hythecraft.hythemain.core.items.ItemManager;
import com.hythecraft.hythemain.core.items.types.useable.WeaponItem;

public class ListenerEntityDamageByEntityEvent implements Listener{
	
	@EventHandler
	public void execute(EntityDamageByEntityEvent e) {
		if(e.getEntity() == null)
			return;
		if(e.getEntity() instanceof ArmorStand)
			return;
		
		if(e.getDamager() instanceof Player) {
			if(((Player)e.getDamager()).getInventory().getItemInMainHand() == null) return;
			if(((Player)e.getDamager()).getInventory().getItemInMainHand().getType() != Material.AIR)
			{
				ItemStack item = ((Player)e.getDamager()).getInventory().getItemInMainHand();
				if(ItemManager.getCustomItemHandler(ItemManager.getCustomItemType(item)) instanceof WeaponItem)
				{
					WeaponItem weapon = (WeaponItem)ItemManager.getCustomItemHandler(ItemManager.getCustomItemType(item));
					new AttackInstance((Player)e.getDamager(), (LivingEntity) e.getEntity(), item, weapon.damage());
				}
			}	
		}
		
	}
}

