package com.hythecraft.hythemain.core.listener.listeners.entity;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;

public class ListenerEntityPickupItemEvent implements Listener{
    @EventHandler
    public void execute(EntityPickupItemEvent e) {
    	if(!(e.getEntity() instanceof Player)) return;
    	Player player = (Player)e.getEntity();
    	player.updateInventory();
    }
}
