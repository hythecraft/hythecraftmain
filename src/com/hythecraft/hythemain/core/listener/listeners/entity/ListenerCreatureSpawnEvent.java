package com.hythecraft.hythemain.core.listener.listeners.entity;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

import com.google.common.collect.ImmutableSet;

public class ListenerCreatureSpawnEvent implements Listener{
	public final ImmutableSet<EntityType> entityBlackList = ImmutableSet.<EntityType>builder()
			.add(EntityType.WANDERING_TRADER)
			.add(EntityType.TRADER_LLAMA)
			.add(EntityType.PHANTOM)
			.add(EntityType.PILLAGER)
			.add(EntityType.SKELETON_HORSE)
			.build();

	@EventHandler
	public void execute(CreatureSpawnEvent e) {
		//CANCEL ALL NATURAL SPAWNS
		if(e.getSpawnReason() == SpawnReason.NATURAL)
		{
			e.setCancelled(true);
		}
		if(entityBlackList.contains(e.getEntityType()))
		{
			e.setCancelled(true);
		}
	}
}
