package com.hythecraft.hythemain.core.listener.listeners.entity;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import net.citizensnpcs.api.CitizensAPI;

public class ListenerEntityDamageEvent implements Listener
{
	@EventHandler
	public void execute(EntityDamageEvent e) {
		List<Player> pl = new ArrayList<Player>();
		CitizensAPI.getNPCRegistry().forEach(npc -> pl.add((Player)npc.getEntity()));
        if (e.getEntity() instanceof Player){
        	{
        		if(!pl.contains(e.getEntity()))
        			e.setCancelled(true);
        	}
        }
	}
}
