package com.hythecraft.hythemain.core.listener.listeners.entity;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;

public class ListenerEntityShootBowEvent implements Listener {
	   HashMap<Player, ItemStack> storedItem = new HashMap<Player, ItemStack>();

	   @EventHandler
	   public void PlayerItemHeldEvent(PlayerItemHeldEvent e) {
	     returnItem(e.getPlayer());
	   }

	   @EventHandler
	   public void PlayerDropItemEvent(PlayerDropItemEvent e) {
	     returnItem(e.getPlayer());
	   }
	 
	   @EventHandler
	   public void EntityShootBowEvent(EntityShootBowEvent e)
	   {
	     if(e.getEntity() instanceof Player)
	     {
	       returnItem((Player) e.getEntity());
	     }

	   }
	   private void returnItem(Player player) {
	     if (storedItem.containsKey(player.getPlayer())) {
	       int slot = player.getInventory().getSize() - 1;
	       player.getInventory().setItem(slot, storedItem.get(player));
	       player.updateInventory();
	       storedItem.remove(player);
	     }

	   }

	   @EventHandler
	   public void PlayerInteractEvent(PlayerInteractEvent e) {
	     Player p = e.getPlayer();
	     if (!(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))) return;
	     if(storedItem.containsKey(p)) return;
	     if(!(p.getInventory().getItemInMainHand().getType().equals(Material.BOW))) return;
	     int slot = p.getInventory().getSize() - 1;
	     ItemStack item = p.getInventory().getItem(slot);
	     storedItem.put(p, item);
	     p.getInventory().setItem(slot, new ItemStack(Material.ARROW, 1));
	   }
	}



















	 