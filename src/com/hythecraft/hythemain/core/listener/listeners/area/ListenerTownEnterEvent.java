package com.hythecraft.hythemain.core.listener.listeners.area;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.hythecraft.hythemain.core.listener.events.area.TownEnterEvent;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;

public class ListenerTownEnterEvent implements Listener
{
	@EventHandler
	public void execute(TownEnterEvent e) {
		ProfileData profile = UserManager.getUser(e.getPlayer()).getSelectedProfile();
		profile.setLastTown(e.getTown().getAreaID());
	}
}
