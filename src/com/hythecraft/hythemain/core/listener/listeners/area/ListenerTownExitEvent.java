package com.hythecraft.hythemain.core.listener.listeners.area;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.hythecraft.hythemain.core.listener.events.area.TownExitEvent;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;

public class ListenerTownExitEvent implements Listener{
	@EventHandler
	public void main(TownExitEvent e){
		ProfileData profile = UserManager.getUser(e.getPlayer()).getSelectedProfile();
		profile.setLastTown(null);
	}
}
