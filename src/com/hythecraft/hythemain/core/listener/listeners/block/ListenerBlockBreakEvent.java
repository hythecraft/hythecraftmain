package com.hythecraft.hythemain.core.listener.listeners.block;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class ListenerBlockBreakEvent implements Listener{
	
    @EventHandler
    public void execute(BlockBreakEvent e){
        if(e.getPlayer().hasPermission("hythecraft.admin"))
        	return;
        e.setCancelled(true);
    }
}