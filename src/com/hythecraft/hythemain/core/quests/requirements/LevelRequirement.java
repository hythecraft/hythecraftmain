package com.hythecraft.hythemain.core.quests.requirements;

import com.hythecraft.hythemain.core.quests.requirements.util.QuestRequirement;
import com.hythecraft.hythemain.core.user.ProfileData;

public class LevelRequirement extends QuestRequirement{

	private int minLevel;
	
	public LevelRequirement(int minLevel) {
		this.minLevel = minLevel;
	}
	
	@Override
	public boolean fitReq(ProfileData user) {
		return user.getLevel() >= minLevel;
	}

}
