package com.hythecraft.hythemain.core.quests.requirements.util;

import com.hythecraft.hythemain.core.user.ProfileData;

public abstract class QuestRequirement {
	
	public abstract boolean fitReq(ProfileData user);
}
