package com.hythecraft.hythemain.core.quests.tutorial;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import com.destroystokyo.paper.event.player.PlayerJumpEvent;
import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.quests.requirements.LevelRequirement;
import com.hythecraft.hythemain.core.quests.util.QuestBase;

public class Tutorial2Quest extends QuestBase{
	
	public Tutorial2Quest() {
		super("TUT_QUEST2", "Jump 3 times", "", false, 3, null);
		this.addQuestReq(new LevelRequirement(10));
	}

	@Override
	public void finishQuest(Player player) {
		player.sendMessage("You have jumped enough times");
		
	}

	@EventHandler
	public void jumpEvent(PlayerJumpEvent e) {
		if(!applicablePlayer(e.getPlayer())) return;
		HytheCraft.getInstance().getQuestManager().updateQuest(this.getQuestID(), e.getPlayer(), 1);
	}

	@Override
	public void updateQuest(Player player, int value) {
		player.sendMessage("You have jumped " + value + " times");
		
	}
	
}
