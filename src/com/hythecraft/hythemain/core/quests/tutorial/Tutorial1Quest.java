package com.hythecraft.hythemain.core.quests.tutorial;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import com.destroystokyo.paper.event.player.PlayerJumpEvent;
import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.quests.util.QuestBase;

public class Tutorial1Quest extends QuestBase{
	
	public Tutorial1Quest() {
		super("TUT_QUEST1", "Jump 3 times", "&7Jump 3 times bruh", false, 3, null);
	}

	@Override
	public void finishQuest(Player player) {
		player.sendMessage("You have jumped enough times");
		
	}

	@EventHandler
	public void jumpEvent(PlayerJumpEvent e) {
		if(!applicablePlayer(e.getPlayer())) return;
		HytheCraft.getInstance().getQuestManager().updateQuest(this.getQuestID(), e.getPlayer(), 1);
	}

	@Override
	public void updateQuest(Player player, int value) {
		player.sendMessage("You have jumped " + value + " times");
		
	}
	
}
