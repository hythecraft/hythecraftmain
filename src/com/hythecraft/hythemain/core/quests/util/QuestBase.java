package com.hythecraft.hythemain.core.quests.util;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.quests.requirements.util.QuestRequirement;
import com.hythecraft.hythemain.core.user.UserManager;

import lombok.Getter;

public abstract class QuestBase implements Listener{

	@Getter private String name;
	@Getter private String questID;
	@Getter private String questDescription;
	@Getter private String[] questRewards;
	@Getter private boolean isRepeatable;
	@Getter private int questFinishReq;
	@Getter private List<QuestRequirement> questReqs = new ArrayList<>();
	
	public QuestBase(String questID, String name, String questDescription, boolean repeatable, int finishReq, String... rewards)
	{
		HytheCraft.getInstance().registerListener(this);
		this.name = name;
		this.questID = questID;
		this.questDescription = questDescription;
		this.isRepeatable = repeatable;
		this.questFinishReq = finishReq;
		this.questRewards = rewards;
	}
	
	public abstract void finishQuest(Player player);
	public abstract void updateQuest(Player player, int value);
	
	protected boolean applicablePlayer(Player player) {
		if(HytheCraft.getInstance().getQuestManager().finishedQuest(this.questID, player)) return false;
		if(HytheCraft.getInstance().getQuestManager().inProgressQuest(this.questID, player)) 
			return true;
		return false;
	}
	
	public boolean meetReqs(Player player) {
		for(QuestRequirement quest : questReqs) {
			if(!quest.fitReq(UserManager.getUser(player).getSelectedProfile()))
				return false;
		}
		return true;
	}
	
	protected void addQuestReq(QuestRequirement req) {
		questReqs.add(req);
	}
	
}
