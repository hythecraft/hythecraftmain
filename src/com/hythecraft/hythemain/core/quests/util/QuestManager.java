package com.hythecraft.hythemain.core.quests.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.quests.tutorial.Tutorial1Quest;
import com.hythecraft.hythemain.core.quests.tutorial.Tutorial2Quest;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserData;
import com.hythecraft.hythemain.core.user.UserManager;

import lombok.Getter;

public class QuestManager {

	@Getter static Map<String, QuestBase> questRegistry = new HashMap<>();
	
	public QuestManager() {
		//Tutorial Quests
		registerQuest(new Tutorial1Quest());
		registerQuest(new Tutorial2Quest());
	}
	
	public static void registerQuest(QuestBase base) {
		questRegistry.put(base.getQuestID().toUpperCase(), base);
	}
	
	public List<QuestBase> getAvailableQuests(Player player) {
		List<QuestBase> availableQuests = new ArrayList<>();
		for(Entry<String, QuestBase> entry : questRegistry.entrySet())
		{
			if(entry.getValue().meetReqs(player))
				availableQuests.add(entry.getValue());
		}
		return availableQuests;
	}
	
	public void updateQuest(String questID, Player player, int value)
	{
		UserData user = UserManager.getUser(player);
		ProfileData p = user.getSelectedProfile();
		Map<String, Integer> map = p.getQuests();
		if(!map.containsKey(questID))
			map.put(questID, value);
		else
			map.put(questID, map.get(questID)+value);
		
		QuestBase quest = questRegistry.get(questID);
		quest.updateQuest(player, map.get(questID));
		if(map.get(questID) >= quest.getQuestFinishReq())
		{
			quest.finishQuest(player);
			map.put(questID, -1);
		}
		
		p.setQuests(map);
		
		
	}
	
	public void startQuest(String questID, Player player) {
		UserData user = UserManager.getUser(player);
		ProfileData p = user.getSelectedProfile();
		if(p.getQuests() == null) p.setQuests(new HashMap<>());
		p.getQuests().put(questID, 0);
	}
	
	public static boolean questExists(String questID) {
		if(questRegistry.containsKey(questID.toUpperCase())) return true;
		return false;
	}
	
	public boolean finishedQuest(String questID, Player player) {
		UserData user = UserManager.getUser(player);
		ProfileData p = user.getSelectedProfile();
		if(!inProgressQuest(questID, player)) return false;
		return p.getQuests().get(questID) == -1 ? true : false;
	}
	
	/*
	 * Returns whether the player is currently achieving a quest
	 */
	public boolean inProgressQuest(String questID, Player player) {
		UserData user = UserManager.getUser(player);
		ProfileData p = user.getSelectedProfile();
		if(p.getQuests() == null) return false;
		return p.getQuests().containsKey(questID);
	}
	
	public boolean canStartQuest(String questID, Player player) {
		for(QuestBase q : getAvailableQuests(player))
		{
			if(q.getQuestID().equalsIgnoreCase(questID))
				return true;
		}
		return false;
	}
	
}
