package com.hythecraft.hythemain.core.fx.particles;

import org.bukkit.Location;
import org.bukkit.Particle;

import com.hythecraft.hythemain.core.fx.particles.util.ParticleBase;

public class TrailBlazingParticleEffect extends ParticleBase{
	
	@Override
	public void playEffect(Location loc, double size)
	{
		loc.getWorld().spawnParticle(Particle.FLAME,
				loc,
				0,
				loc.getX(),
				loc.getY(),
				loc.getZ());
		/*new BukkitRunnable() {
			public void run() {
				loc.getWorld().spawnParticle(Particle.CAMPFIRE_COSY_SMOKE,
						loc,
						0,
						loc.getX(),
						loc.getY(),
						loc.getZ());
			}
		}.runTaskLater(HytheCraft.getInstance(), 1);
	*/
	}
}
