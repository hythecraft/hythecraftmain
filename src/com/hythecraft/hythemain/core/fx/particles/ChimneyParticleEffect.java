package com.hythecraft.hythemain.core.fx.particles;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.fx.particles.util.ParticleBase;
import com.hythecraft.hythemain.core.world.weather.WeatherManager;
import com.hythecraft.hythemain.util.NumberUtil;

public class ChimneyParticleEffect extends ParticleBase{
	
	@Override
	public void playEffect(Location loc, double size)
	{
		WeatherManager weather = HytheCraft.getInstance().getWeatherManager();
		
		float direction = NumberUtil.clamp(NumberUtil.clampRadius(weather.getWindDirection()), 0, 360);
		float speed = NumberUtil.clamp(weather.getWindSpeed(), 0, 1); //1 STRONG
		
		Vector rand = new Vector(NumberUtil.random(-0.3f, .3f), 0, NumberUtil.random(-0.3f,  .3f));
		loc.add(rand);
		
		double xpos = speed*2 * Math.cos(direction*(Math.PI/180));
		double zpos = speed*2 * Math.sin(direction*(Math.PI/180));
		
		double ypos = size / 1 - speed;
		
		loc.getWorld().spawnParticle(Particle.CAMPFIRE_COSY_SMOKE,
				loc,
				0,
				xpos,
				ypos,
				zpos);
	}
}
