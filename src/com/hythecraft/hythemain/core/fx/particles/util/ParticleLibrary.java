package com.hythecraft.hythemain.core.fx.particles.util;

import org.bukkit.Location;

import com.hythecraft.hythemain.core.fx.particles.ChimneyParticleEffect;
import com.hythecraft.hythemain.core.fx.particles.OminousParticleEffect;
import com.hythecraft.hythemain.core.fx.particles.TrailBlazingParticleEffect;

import lombok.Getter;

public enum ParticleLibrary {
	
	CHIMNEY(new ChimneyParticleEffect(), "CHIMNEY"),
	OMINOUS(new OminousParticleEffect(), "OMINOUS"),
	TRAILBLAZING(new TrailBlazingParticleEffect(), "TRAILBLAZING");
	
	@Getter private String particleID;
	@Getter private ParticleBase base;
	
	public void playEffect(Location loc, double size) {
		base.playEffect(loc, size);
	}
	
	ParticleLibrary(ParticleBase base, String particleID)
	{
		this.base = base;
		this.particleID = particleID;
	}
}
