package com.hythecraft.hythemain.core.fx.particles.util;

import org.bukkit.Location;

public abstract class ParticleBase {

	public abstract void playEffect(Location loc, double size);
}
