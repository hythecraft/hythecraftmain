package com.hythecraft.hythemain.core.fx.particles;

import org.bukkit.Location;
import org.bukkit.Particle;

import com.hythecraft.hythemain.core.fx.particles.util.ParticleBase;

public class OminousParticleEffect extends ParticleBase{
	
	@Override
	public void playEffect(Location loc, double size)
	{
		for(int i = 0; i < 100; i++) {
		loc.getWorld().spawnParticle(Particle.ENCHANTMENT_TABLE,
				loc,
				(int) (5 * size),
				loc.getX(),
				loc.getY(),
					loc.getZ(),
				3);
		}
	}
}
