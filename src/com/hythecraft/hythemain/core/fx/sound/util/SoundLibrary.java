package com.hythecraft.hythemain.core.fx.sound.util;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.fx.sound.WaterFallSoundEffect;

import lombok.Getter;

public enum SoundLibrary {
	
	WATERFALL(new WaterFallSoundEffect(), "WATERFALL");
	
	@Getter private String soundID;
	@Getter private SoundEffectBase base;
	
	public void playEffect(Location loc, Player... players) {
		base.playSound(loc, players);
	}
	
	SoundLibrary(SoundEffectBase base, String soundID)
	{
		this.base = base;
		this.soundID = soundID;
	}
}
