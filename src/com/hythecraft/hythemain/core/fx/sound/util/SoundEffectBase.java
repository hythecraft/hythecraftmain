package com.hythecraft.hythemain.core.fx.sound.util;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public abstract class SoundEffectBase {
	
	public abstract void playSound(Location loc, Player... players);
	
}
