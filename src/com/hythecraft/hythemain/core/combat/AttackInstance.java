package com.hythecraft.hythemain.core.combat;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.hythecraft.hythemain.core.items.ItemManager;
import com.hythecraft.hythemain.core.items.ModifierManager;
import com.hythecraft.hythemain.core.items.types.useable.WeaponItem;
import com.hythecraft.hythemain.core.user.UserManager;

public class AttackInstance {

	public int damage = 0;
	LivingEntity damaging;
	Player damager;
	
	public AttackInstance(Player damager, LivingEntity damaging, ItemStack item, float modifier) {
		if(damaging instanceof ArmorStand) return;
		if(!(damager instanceof Player)) return;
		if(!ItemManager.canUseItem(damager, item)) return;
		if(!(ItemManager.getCustomItemHandler(ItemManager.getCustomItemType(item)) instanceof WeaponItem)) return;
		
		WeaponItem weapon = (WeaponItem)ItemManager.getCustomItemHandler(ItemManager.getCustomItemType(item));
		
		this.damaging = damaging;
		damage = (int) (modifier * DamageCalc.getDamageToEnemy(UserManager.getUser(damager).getSelectedProfile(), (double)(weapon).damage()));
		ModifierManager.applyCombatMod(this, ModifierManager.getCombatMods(item));
		damage();
	}
	
	public void damage() {
		damaging.damage(damage);
			
	}
}
	
 