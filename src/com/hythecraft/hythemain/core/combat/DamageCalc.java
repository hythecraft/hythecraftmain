package com.hythecraft.hythemain.core.combat;

import com.hythecraft.hythemain.core.user.ProfileData;

public class DamageCalc {
	
	public static double getDamageToEnemy(ProfileData player, double obj) {
		return obj;
	}
	
	public static double getDamageToPlayer(ProfileData player, double damage) {
		return damage*(100/(100+(double)player.getDefense()) );
	}
	
}
