package com.hythecraft.hythemain.core.permission;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.permission.listener.Permission_ListenerPlayerJoinEvent;
import com.hythecraft.hythemain.core.user.UserData;
import com.hythecraft.hythemain.core.user.UserManager;

/*
 * Probably should be it's own plugin
 */
public class PlayerPermissionManager {
	public static PermissionGroup defaultGroup = PermissionGroup.DEFAULT;
	
	public static HashMap<UUID,PermissionAttachment> permissionList = new HashMap<>();
	
	public PlayerPermissionManager() {
		registerListeners();
	}
	
	public void registerListeners() {
		HytheCraft.getInstance().registerListener(
				new Permission_ListenerPlayerJoinEvent()
		);
	}
	
	public void removePermission(Player player, String perm) {
		permissionList.get(player.getUniqueId()).unsetPermission(perm);
	}
	
	public void setPermission(Player player, String perm) {
		setPermission(player, perm, true);
	}
	
	public void setPermission(Player player, String perm, boolean value) {
		if(!permissionList.containsKey(player.getUniqueId())) return;
		permissionList.get(player.getUniqueId()).setPermission(perm, value);
	}
	
	public void setPlayerGroup(Player player, PermissionGroup group) {
		UserData user = UserManager.getUser(player);
		user.setGroup(group);
		assignPermissions(player);
	}
	
	/*
	 * Called on join
	 */
	public void assignPermissions(Player player) {
		UserData user = UserManager.getUser(player);
		if(user.getGroup() == null) user.setGroup(defaultGroup);
		for(String str : user.getGroup().getAllPerms())
		{
			setPermission(player, str);
		}
	}
	
}














