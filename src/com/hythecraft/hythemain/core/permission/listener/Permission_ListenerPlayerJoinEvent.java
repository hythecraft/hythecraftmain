package com.hythecraft.hythemain.core.permission.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.PermissionAttachment;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.permission.PlayerPermissionManager;

public class Permission_ListenerPlayerJoinEvent implements Listener
{
	@EventHandler
	public void execute(PlayerJoinEvent e) {	
		Player player = e.getPlayer();
		PermissionAttachment attachment = player.addAttachment(HytheCraft.getInstance());
		PlayerPermissionManager.permissionList.put(player.getUniqueId(), attachment);
		
		HytheCraft.getInstance().getPlayerPermissionManager().assignPermissions(player);
	}
}
