package com.hythecraft.hythemain.core.characters;

import lombok.Getter;

public enum Characters {

	ULRIC("ULRIC", "&7Ulric", "Your shipmate");
	
	@Getter private String ID;
	@Getter private String displayName;
	@Getter private String[] description;
	
	private Characters(String ID, String displayName, String... description)
	{
		this.ID = ID;
		this .displayName = displayName;
		this.description = description;
		
	}
	
	public static Characters getChar(String ID) {
		for(Characters c : Characters.values())
		{
			if(c.getID().equals(ID))
				return c;
		}
		return null;
	}
}
