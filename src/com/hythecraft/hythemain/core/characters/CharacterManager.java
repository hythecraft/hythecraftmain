package com.hythecraft.hythemain.core.characters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;

public class CharacterManager {
	
	public void discoverCharacter(Player player, Characters character) {
		ProfileData profile = UserManager.getUser(player).getSelectedProfile();
		if(profile.getCharactersFound() == null) profile.setCharactersFound(new ArrayList<>());
		if(profile.getCharactersFound().contains(character.getID())) return;
		
		List<String> characts = profile.getCharactersFound();
		characts.add(character.getID());
		profile.setCharactersFound(characts);
		
	}
	
	public Characters[] getDiscovered(Player player) {
		ProfileData profile = UserManager.getUser(player).getSelectedProfile();
		List<Characters> characters = new ArrayList<>();
		for(String id : profile.getCharactersFound())
		{
			characters.add(Characters.getChar(id));
		}
		return characters.toArray(new Characters[characters.size()]);
	}
	
}
