package com.hythecraft.hythemain.core.area.town.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.area.Area;
import com.hythecraft.hythemain.core.listener.events.area.TownEnterEvent;
import com.hythecraft.hythemain.core.listener.events.area.TownExitEvent;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.Colors;
import com.hythecraft.hythemain.util.location.LocationUtil;
import com.hythecraft.hythemain.util.location.Vector2;

import lombok.Getter;

public class TownBase extends Area{
	@Getter private Location[] spawnPoints;
	
	public TownBase(String areaID, String name, Vector2 bound1, Vector2 bound2, Location... spawnPoints)
	{
		super(areaID, name, bound1, bound2);
		this.spawnPoints = spawnPoints;
		HytheCraft.getInstance().registerListener(this);
	}
	
	@Override
	public void onEnter(Player pl) {
		pl.sendTitle(Colors.parseColors("&6Now entering"), Colors.parseColors(name), 20, 20, 20);
		Bukkit.getPluginManager().callEvent(new TownEnterEvent(pl, this));
	}
	
	@Override
	public void onExit(Player pl) {
		pl.sendTitle(Colors.parseColors("&6Now leaving"), Colors.parseColors(name), 20, 20, 20);
		Bukkit.getPluginManager().callEvent(new TownExitEvent(pl, this));
	}
	
	@EventHandler
	public void execute(PlayerMoveEvent e) {
		ProfileData profile = UserManager.getUser(e.getPlayer()).getSelectedProfile();
		if(profile == null) return;
		if(LocationUtil.withinBounds(e.getPlayer().getLocation(), bound1, bound2))
		{
			if(profile.getLastTown() == null || !(profile.getLastTown().equals(areaID)))
				onEnter(e.getPlayer());
		}
		else if(profile.getLastTown() != null)
		{
			if(profile.getLastTown().equals(areaID))
				onExit(e.getPlayer());
		}
	}
}
