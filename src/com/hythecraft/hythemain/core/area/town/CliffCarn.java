package com.hythecraft.hythemain.core.area.town;

import com.hythecraft.hythemain.core.area.town.util.TownBase;
import com.hythecraft.hythemain.util.location.Vector2;

public class CliffCarn extends TownBase{
	
	public CliffCarn() {
		super("CLIFFCARN", "Cliffcarn", new Vector2(-61, 2), new Vector2(-65, 4), null);
	}

}
