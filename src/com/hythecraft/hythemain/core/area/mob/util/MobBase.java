package com.hythecraft.hythemain.core.area.mob.util;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.area.Area;
import com.hythecraft.hythemain.core.entity.HytheEntity.HytheEntityType;
import com.hythecraft.hythemain.core.listener.events.updates.UpdateSecondEvent;
import com.hythecraft.hythemain.util.NumberUtil;
import com.hythecraft.hythemain.util.location.LocationUtil;
import com.hythecraft.hythemain.util.location.Vector2;

import lombok.Getter;

public class MobBase extends Area{

	@Getter private Location[] spawnPoints;
	@Getter private int areaLevel;
	@Getter private HytheEntityType mobType; 		
	@Getter private int maxMobCount;
	
	protected List<Entity> entities = new ArrayList<>();
	protected List<Player> players = new ArrayList<>();
	
	public MobBase(String areaID, String name, Vector2 bound1, Vector2 bound2, HytheEntityType mobType, int areaLevel, int maxMobCount, Location... spawnPoints) {
		super(areaID, name, bound1, bound2);
		this.areaLevel = areaLevel;
		this.mobType = mobType;
		this.maxMobCount = maxMobCount;
		this.spawnPoints = spawnPoints;
		HytheCraft.getInstance().registerListener(this);
	}
	
	private int counter = 0;
	
	@EventHandler
	public void second(UpdateSecondEvent e) {
		if(counter < 3) {counter++; return;}
		if(players.size() == 0) {
			for(Entity ent : entities)
			{
				ent.remove();
			}
			entities = new ArrayList<>();
		}
		else{
			final Entity ent = mobType.spawn(spawnPoints[NumberUtil.random(0, spawnPoints.length-1)], areaLevel).getBukkitEntity();
			entities.add(ent);
		}
		
		counter = 0;
	}
	
	@EventHandler
	public void entityDeath(EntityDeathEvent e) {
		if(!entities.contains(e.getEntity())) return;
		entities.remove(e.getEntity());
	}
	
	@EventHandler
	public void execute(PlayerMoveEvent e) {
		if(LocationUtil.withinBounds(e.getPlayer().getLocation(), bound1, bound2))
			onEnter(e.getPlayer());
		else
			onExit(e.getPlayer());
	}
	
	@Override
	public void onEnter(Player pl) {
		if(players.contains(pl)) return;
		players.add(pl);
		
	}

	@Override
	public void onExit(Player pl) {
		if(!players.contains(pl)) return;
		players.remove(pl);
	}

}
