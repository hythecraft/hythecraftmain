package com.hythecraft.hythemain.core.area;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.hythecraft.hythemain.util.location.LocationUtil;
import com.hythecraft.hythemain.util.location.Vector2;

import lombok.Getter;

public abstract class Area implements Listener{
	@Getter public Vector2 bound1, bound2;
	@Getter public String areaID;
	@Getter public String name;
	
	public Area(String areaID, String name, Vector2 bound1, Vector2 bound2)
	{
		this.areaID = areaID;
		this.name = name;
		this.bound1 = bound1;
		this.bound2 = bound2;
	}

	public boolean withinBounds(Player player) {
		return LocationUtil.withinBounds(player.getLocation(), bound1, bound2);
	}
	
	
	public abstract void onEnter(Player pl);
	
	public abstract void onExit(Player pl);
}
