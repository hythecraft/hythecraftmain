package com.hythecraft.hythemain.core.dialogue.dialogues.tutorial;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.dialogue.util.DialogueOption;
import com.hythecraft.hythemain.core.dialogue.util.dialoguetypes.ChoiceDialogueBase;

public class Tutorial1Dialogue extends ChoiceDialogueBase{

	public Tutorial1Dialogue() {
		super("TUT_DIALOGUE1", 
				"&f&lWandering Trader",
				"&7Hey %PLAYER%! how can i help you?", 
				new DialogueOption("&aHow do I get to town? ", new Tutorial2Dialogue()),
				new DialogueOption("&aWho are you?", new Tutorial3Dialogue()));
	}

	@Override
	public void startDialogue(Player player) {
	}

	@Override
	public void endDialogue(Player player) {
	}

}
