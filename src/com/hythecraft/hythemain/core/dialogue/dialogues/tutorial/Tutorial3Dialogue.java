package com.hythecraft.hythemain.core.dialogue.dialogues.tutorial;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.dialogue.util.DialogueText;
import com.hythecraft.hythemain.core.dialogue.util.dialoguetypes.LinearDialogueBase;

public class Tutorial3Dialogue extends LinearDialogueBase{

	public Tutorial3Dialogue() {
		super("TUT_DIALOGUE3", 
				new DialogueText("&f&lThe Wandering Trader", "&7I am &f&lThe Wandering Trader&7!"),
				new DialogueText("&f&lThe Wandering Trader", "&7This is a test dialogue!"));
	}

	@Override
	public void startDialogue(Player player) {
	}

	@Override
	public void endDialogue(Player player) {
	}

}
