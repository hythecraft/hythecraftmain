package com.hythecraft.hythemain.core.dialogue.dialogues.tutorial;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.dialogue.util.DialogueText;
import com.hythecraft.hythemain.core.dialogue.util.dialoguetypes.LinearDialogueBase;
import com.hythecraft.hythemain.util.unicode.ProfileRepo;

public class Tutorial2Dialogue extends LinearDialogueBase{

	public Tutorial2Dialogue() {
		super("TUT_DIALOGUE2", 
				new DialogueText("&f&lThe Wandering Trader", "&7Hello %PLAYER%!"),
				new DialogueText("&f&lThe Wandering Trader", "&7Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."));
		this.setCharID(ProfileRepo.WANDERING_TRADER);
	}

	@Override
	public void startDialogue(Player player) {
	}

	@Override
	public void endDialogue(Player player) {
	}

}
