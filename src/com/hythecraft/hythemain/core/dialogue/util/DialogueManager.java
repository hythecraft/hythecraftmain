package com.hythecraft.hythemain.core.dialogue.util;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.dialogue.dialogues.tutorial.Tutorial1Dialogue;
import com.hythecraft.hythemain.core.dialogue.dialogues.tutorial.Tutorial2Dialogue;
import com.hythecraft.hythemain.core.dialogue.dialogues.tutorial.Tutorial3Dialogue;
import com.hythecraft.hythemain.core.dialogue.util.dialoguetypes.DialogueBase;

import lombok.Getter;

public class DialogueManager {

	@Getter static Map<String, DialogueBase> dialogueRegistry = new HashMap<>();
	
	public DialogueManager() {
		registerDialogue(new Tutorial1Dialogue());
		registerDialogue(new Tutorial2Dialogue());
		registerDialogue(new Tutorial3Dialogue());
	}
	
	public static void registerDialogue(DialogueBase base) {
		dialogueRegistry.put(base.getDialogueID().toUpperCase(), base);
	}
	
	public void startDialogue(String dialogueID, Player player)
	{
		getDialogue(dialogueID).playDialogue(player);
	}
	
	public void startDialogue(DialogueBase base, Player player) {
		base.playDialogue(player);
	}
	
	public DialogueBase getDialogue(String dialogueID) {
		if(!dialogueExists(dialogueID)) return null;
		return dialogueRegistry.get(dialogueID);
	}
	
	public static boolean dialogueExists(String dialogueID)
	{
		if(dialogueRegistry.containsKey(dialogueID.toUpperCase())) return true;
		
		return false;
	}
	
}
