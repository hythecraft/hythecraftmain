package com.hythecraft.hythemain.core.dialogue.util.dialoguetypes;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.dialogue.util.DialogueOption;
import com.hythecraft.hythemain.util.Colors;

import lombok.Getter;
import lombok.Setter;

/*
 * A Dialogue type that gives players the option to choose their input
 * 
 */
public abstract class ChoiceDialogueBase extends DialogueBase{
	@Getter private DialogueOption[] content;
	@Getter private String initialText;
	@Getter private String prefix;
	@Getter @Setter private boolean silent = false;
	
	/*
	 * @param initialText the text sent before the content options, not clickable
	 */
	public ChoiceDialogueBase(String dialogueID, String prefix, String initialText, DialogueOption... content)
	{
		super(dialogueID);
		this.prefix = prefix;
		this.content = content;
		this.initialText = initialText;
	}
	
	@Override
	public void playDialogue(Player player, long time)
	{
		int index = 0;
		startDialogue(player);
		player.sendMessage(Colors.parseColors(prefix));
		player.sendMessage(Colors.parseColors(initialText.replace("%PLAYER%", player.getDisplayName())));

		for(DialogueOption o : content)
		{
			o.send(player, index++);
		}
		endDialogue(player);
		

		if(!silent)
			player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_TRADE, 10, 1);
	}
	
}








