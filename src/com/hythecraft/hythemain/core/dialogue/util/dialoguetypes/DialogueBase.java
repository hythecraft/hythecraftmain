package com.hythecraft.hythemain.core.dialogue.util.dialoguetypes;

import org.bukkit.entity.Player;

import lombok.Getter;

public abstract class DialogueBase {
	@Getter private String dialogueID;
	
	public DialogueBase(String dialogueID) {
		this.dialogueID = dialogueID;
	}
	
	protected final long dialogueSpeed = 3; //Default dialogue speed
	
	public abstract void playDialogue(Player player, long time);
	public void playDialogue(Player player)
	{
		playDialogue(player, dialogueSpeed);
	}
	
	protected abstract void startDialogue(Player player);
	protected abstract void endDialogue(Player player);
}
