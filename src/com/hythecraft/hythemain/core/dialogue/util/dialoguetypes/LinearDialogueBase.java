package com.hythecraft.hythemain.core.dialogue.util.dialoguetypes;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.dialogue.util.DialogueText;
import com.hythecraft.hythemain.util.unicode.ProfileRepo;

import lombok.Getter;
import lombok.Setter;

/*
 * Basic dialogue type that outputs a String collection with delay
 */
public abstract class LinearDialogueBase extends DialogueBase{
	@Getter private DialogueText[] content;
	@Getter @Setter private ProfileRepo charID;
	
	public LinearDialogueBase(String dialogueID, DialogueText... content)
	{
		super(dialogueID);
		this.content = content;
	}
	
	@Override
	public void playDialogue(Player player, long time)
	{
		new BukkitRunnable() {
			int index = 0;
            public void run() {
            	if(content.length == index)
            	{
            		LinearDialogueBase.this.endDialogue(player);
            		this.cancel();
            		return;
            	}
            	content[index].send(player, index + 1, content.length, charID);
        		player.sendMessage(" ");
            	index++;
            }
        }.runTaskTimer(HytheCraft.getInstance(), 0, time * 20);
        
		startDialogue(player);
	}
	
}








