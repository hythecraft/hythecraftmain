package com.hythecraft.hythemain.core.dialogue.util.dialoguetypes;

import org.bukkit.entity.Player;
/*
 * Generally endpoint of a ChoiceDialogueBase
 */
public abstract class DialogueOutput extends DialogueBase{

	public DialogueOutput(String dialogueID) {
		super(dialogueID);
	}
	
	protected abstract void startDialogue(Player player);
	
	

}
