package com.hythecraft.hythemain.core.dialogue.util;

import java.util.List;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.util.Colors;
import com.hythecraft.hythemain.util.gui.TextAlignment;
import com.hythecraft.hythemain.util.unicode.ProfileRepo;

import lombok.Getter;
import lombok.Setter;

public class DialogueText {

	@Getter private String prefix;
	@Getter private String message;
	@Getter @Setter private boolean silent = false;
	@Getter @Setter private ProfileRepo charID;
	
	public DialogueText(String prefix, String message) {
		this.prefix = prefix;
		this.message = message;
	}
	
	public DialogueText(String message) {
		this.prefix = null;
		this.message = message;
	}
	
	public void send(Player player, int index, int maxIndex, ProfileRepo charID) {
		this.charID = charID;
		
		String msg = message.replace("%PLAYER%", player.getDisplayName());
		String indexer = index == 0  ? "" : " &8(" + index + "/" + maxIndex + ")";
		
		if(charID != null) 
		{
			List<String> truncated = TextAlignment.truncateText(50, Colors.parseColors(msg));
		
			player.sendMessage(charID.getBlock()[0] + Colors.parseColors(prefix + indexer)); 
			player.sendMessage(charID.getBlock()[1] + Colors.parseColors( (truncated.size() >= 1 ? truncated.get(0) : "") ));
			player.sendMessage(charID.getBlock()[2] + Colors.parseColors( (truncated.size() >= 2 ? truncated.get(1) : "") ));
			player.sendMessage(charID.getBlock()[3] + Colors.parseColors( (truncated.size() >= 3 ? truncated.get(2) : "") ));
			for(int i = 3; i < truncated.size()-1; i++)
				player.sendMessage(truncated.get(i));
		}
		else {
			player.sendMessage(Colors.parseColors(prefix + indexer));
			player.sendMessage(Colors.parseColors(msg));
		}
		if(!silent)
			player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_TRADE, 10, 1);
	
	}
	
}
