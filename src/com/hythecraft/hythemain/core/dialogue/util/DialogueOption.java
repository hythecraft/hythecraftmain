package com.hythecraft.hythemain.core.dialogue.util;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.command.commands.misc.utility.DialogueUtilCommand;
import com.hythecraft.hythemain.core.dialogue.util.dialoguetypes.DialogueBase;
import com.hythecraft.hythemain.util.Colors;
import com.hythecraft.hythemain.util.JSONUtil;

import lombok.Getter;
import net.md_5.bungee.api.chat.TextComponent;

public class DialogueOption {

	@Getter private String message;
	@Getter private DialogueBase output;
	
	public DialogueOption(String message, DialogueBase output) {
		this.output = output;
		this.message = message;
	}
	
	public void send(Player player) {
		send(player, -1);
	}
	
	public void send(Player player, int index) {
		//CREATE JSON CLICKABLE
		//Note: this is super janky, no other way of doing it currently
		TextComponent prefix = new TextComponent(Colors.parseColors(index == -1 ? "" : "&7"+ index + "&f. "));
		TextComponent body = new TextComponent(JSONUtil.getJSON(Colors.parseColors(message), DialogueUtilCommand.DIAG_CMD + " " + output.getDialogueID() + " " + HytheCraft.getSessionSecret(), false, ""));
		player.spigot().sendMessage(new TextComponent[] {prefix,body});
		
	}
	
}
