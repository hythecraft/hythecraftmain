package com.hythecraft.hythemain.core.items.impl.consumable;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;

import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.types.useable.ConsumableItem;
import com.hythecraft.hythemain.core.items.util.ItemRarity;
import com.hythecraft.hythemain.core.items.util.ItemRequirement;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.ItemBuilder;
import com.hythecraft.hythemain.util.NumberUtil;
import com.hythecraft.hythemain.util.item.ItemUtil;

public class Sophias_Golden_Nugget extends ConsumableItem{

	public Sophias_Golden_Nugget() {
		super("SOPHIAS_GOLDEN_NUGGET", new ItemBuilder(Material.GOLD_NUGGET).create(), "&6Sophia's Golden nugget", CustomItemType.SOPHIAS_GOLDEN_NUGGET, "&7Heals you for &a100 &7health");
	}
	
	@Override
	public ItemRarity rarity() {
		return ItemRarity.Very_Rare;
	}
	
	@Override
	public void leftClick(PlayerInteractEvent e) {
	}

	@Override
	public void rightClick(PlayerInteractEvent e) {
		if(!isApplicableItem(e.getItem())) return;
		Player player = e.getPlayer();
		ProfileData profile = UserManager.getUser(player).getSelectedProfile();
		
		if(profile.getHealth() == profile.getMaxHealth()) return;
		
		player.getInventory().setItemInMainHand(ItemUtil.removeOrDel(player.getInventory().getItemInMainHand() ,1));
		profile.setHealth((int)NumberUtil.clampMax(profile.getHealth() + 100, profile.getMaxHealth()));
	}

	@Override
	public List<ItemRequirement> requirements() {
		return null;
	}
	
}
















