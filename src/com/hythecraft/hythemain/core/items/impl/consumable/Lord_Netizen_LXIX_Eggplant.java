package com.hythecraft.hythemain.core.items.impl.consumable;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;

import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.types.useable.ConsumableItem;
import com.hythecraft.hythemain.core.items.util.ItemRarity;
import com.hythecraft.hythemain.core.items.util.ItemRequirement;
import com.hythecraft.hythemain.util.ItemBuilder;

public class Lord_Netizen_LXIX_Eggplant extends ConsumableItem{

	public Lord_Netizen_LXIX_Eggplant() {
		super("LORD_NETIZEN_LXIX_EGGPLANT", new ItemBuilder(Material.ACACIA_BOAT).create(), "&cLord Netizen LXIX's Eggplant", CustomItemType.LORD_NETIZEN_LXIX_EGGPLANT, "&7When eaten, gives a temporary armor boost");
	}
	
	@Override
	public ItemRarity rarity() {
		return ItemRarity.Ultra_Rare;
	}
	
	@Override
	public void leftClick(PlayerInteractEvent e) {
	}

	@Override
	public void rightClick(PlayerInteractEvent e) {
		if(!isApplicableItem(e.getItem())) return;
	}

	@Override
	public List<ItemRequirement> requirements() {
		return null;
	}
	
}
















