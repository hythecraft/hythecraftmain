package com.hythecraft.hythemain.core.items.impl.weapon;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.fx.particles.util.ParticleLibrary;
import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.modifiers.ModifierBase;
import com.hythecraft.hythemain.core.items.types.interfaces.InteractableItem;
import com.hythecraft.hythemain.core.items.types.useable.WeaponItem;
import com.hythecraft.hythemain.core.items.util.ItemRarity;
import com.hythecraft.hythemain.core.items.util.ItemRequirement;
import com.hythecraft.hythemain.util.ItemBuilder;

public class A_Very_Useful_Stick extends WeaponItem implements InteractableItem{

	public A_Very_Useful_Stick() {
		super("A_VERY_USEFUL_STICK", new ItemBuilder(Material.STICK).create(), "&dA Very Useful Stick", CustomItemType.A_VERY_USEFUL_STICK, "&7This stick seems to be very useful", "&7When right clicked, a bolt of lightning is struck");
		
	}

	@Override
	public ItemRarity rarity() {
		return ItemRarity.Very_Rare;
	}

	@Override
	public void leftClick(PlayerInteractEvent e) {
		
	}

	@Override
	public void rightClick(PlayerInteractEvent e) {
		if(!isApplicableItem(e.getItem())) return;
		if(!canUse(e.getPlayer())) return;
		
		Location loc = e.getPlayer().getLocation();
		HytheCraft.getDefaultWorld().strikeLightning(loc);
		ParticleLibrary.OMINOUS.playEffect(e.getPlayer().getLocation(), 300);
	}

	@Override
	public List<ItemRequirement> requirements() {
		return null;
	}
	
	@Override
	public List<ModifierBase> modifiers() {
		return null;
	}
	
	@Override
	public int damage() {
		return 100;
	}

}



















