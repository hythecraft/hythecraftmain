package com.hythecraft.hythemain.core.items.impl.armor;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

import com.hythecraft.hythemain.core.fx.particles.util.ParticleLibrary;
import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.modifiers.ModifierBase;
import com.hythecraft.hythemain.core.items.types.useable.DefenseItem;
import com.hythecraft.hythemain.core.items.util.ItemRarity;
import com.hythecraft.hythemain.core.items.util.ItemRequirement;
import com.hythecraft.hythemain.util.ItemBuilder;

public class Trailblazing_Boots extends DefenseItem {
	public Trailblazing_Boots() {
		super("TRAILBLAZING_BOOTS", new ItemBuilder(Material.DIAMOND_BOOTS).create(), "&cTrailblazing Boots", CustomItemType.TRAILBLAZING_BOOTS, "&ehot &cHot &4HOT!");
	}

	@Override
	public ItemRarity rarity() {
		return ItemRarity.Ultra_Rare;
	}

	@Override
	public List<ItemRequirement> requirements() {
		return null;
	}

	@Override
	public int defense() {
		return 100;
	}

	@Override
	public List<ModifierBase> modifiers() {
		return null;
	}
	
	@EventHandler
	public void playerMoveEvent(PlayerMoveEvent e) {
		if(!isEquipped(e.getPlayer())) return;
		ParticleLibrary.TRAILBLAZING.playEffect(e.getPlayer().getLocation().clone().add(0,.1,0), 1);
	}

}



























