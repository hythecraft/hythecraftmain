package com.hythecraft.hythemain.core.items.impl.armor;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;

import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.modifiers.ModifierBase;
import com.hythecraft.hythemain.core.items.modifiers.staticmods.MaximumHealth;
import com.hythecraft.hythemain.core.items.modifiers.staticmods.Speed;
import com.hythecraft.hythemain.core.items.types.useable.DefenseItem;
import com.hythecraft.hythemain.core.items.util.ItemRarity;
import com.hythecraft.hythemain.core.items.util.ItemRequirement;
import com.hythecraft.hythemain.util.ItemBuilder;

public class Bubys_Leggings extends DefenseItem{

	public Bubys_Leggings() {
		super("BUBYS_LEGGINGS", new ItemBuilder(Material.LEATHER_LEGGINGS).create(), "&aBuby's Leggings", CustomItemType.BUBYS_LEGGINGS, "&7Very powerful Leggings");
	}

	@Override
	public ItemRarity rarity() {
		return ItemRarity.Ultra_Rare;
	}

	@Override
	public List<ItemRequirement> requirements() {
		return null;
	}

	@Override
	public int defense() {
		return 100;
	}

	@Override
	public List<ModifierBase> modifiers() {
		return Arrays.asList(new ModifierBase[] {
				new MaximumHealth(1, 100),
				new Speed(1,125)});
	}
	
}
