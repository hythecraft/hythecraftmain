package com.hythecraft.hythemain.core.items.impl.armor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.EquipmentSlot;

import com.destroystokyo.paper.event.player.PlayerArmorChangeEvent;
import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.modifiers.ModifierBase;
import com.hythecraft.hythemain.core.items.types.useable.DefenseItem;
import com.hythecraft.hythemain.core.items.util.ItemRarity;
import com.hythecraft.hythemain.core.items.util.ItemRequirement;
import com.hythecraft.hythemain.util.ItemBuilder;

public class FlowerHat extends DefenseItem{

	public static Map<Player, Entity> armorStands = new HashMap<>();
	
	public FlowerHat() {
		super("FLOWER_HAT", new ItemBuilder(Material.DIAMOND_HELMET).create(), "&dFlower Hat", CustomItemType.FLOWER_HAT, "&7Smells nice");
	}

	@Override
	public ItemRarity rarity() {
		return ItemRarity.Ultra_Rare;
	}

	@Override
	public List<ItemRequirement> requirements() {
		return null;
	}

	@Override
	public int defense() {
		return 100;
	}

	@Override
	public List<ModifierBase> modifiers() {
		return null;
	}
	
	@EventHandler
	public void playerArmorChange(PlayerArmorChangeEvent e) {
		if(!isApplicableItem(e.getOldItem()) && !isApplicableItem(e.getNewItem())) return;
		if(isApplicableItem(e.getOldItem()))
		{
			armorStands.get(e.getPlayer()).remove();
			armorStands.remove(e.getPlayer());
		}
		else
		{
			Location loc = e.getPlayer().getLocation();
			Entity armorStand;
			armorStand = loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
			
			ArmorStand stand = (ArmorStand)armorStand;
			armorStand.setInvulnerable(true);
			stand.setCustomNameVisible(false);
			stand.setVisible(false);
			stand.setGravity(false);
			stand.setItem(EquipmentSlot.HAND, new ItemBuilder(Material.POPPY).create());
			armorStands.put(e.getPlayer(), armorStand);
		}
		
	}
	
	@SuppressWarnings("unused")
	@EventHandler
	public void playerMoveEvent(PlayerMoveEvent e) {
		if(!isEquipped(e.getPlayer())) return;
		if(!armorStands.containsKey(e.getPlayer())) return;
		/*
		 * pitch between 90 and -90; 0 being looking straight
		 */
		double pitch = e.getPlayer().getLocation().getPitch();
		double yaw = e.getPlayer().getLocation().getYaw();
		Bukkit.broadcastMessage(pitch + "");
		
		Location loc = e.getPlayer().getLocation().clone().add(0,1,0);
		Entity ent = armorStands.get(e.getPlayer());
		
		ArmorStand stand = (ArmorStand)ent;
		//double outcomePitch = 
		
		//stand.setRightArmPose(new EulerAngle(Math.toRadians(outcomePitch), Math.toRadians(yaw),0));
		//ent.teleport(loc);
	}
	
	/*
	 * @param z represents angle between 90 to -90 as 1 -1
	 */
	public double f(double x) {
		return (double) (1d*Math.sqrt(Math.pow(1d, 2d) - Math.pow((x), 2d)));
	}

	//derivative
	public double df(double x) {
		return (double) x*(-(x)/Math.sqrt(1d - Math.pow(x,2d)) * (Math.PI/2) );
	}
	
}
































