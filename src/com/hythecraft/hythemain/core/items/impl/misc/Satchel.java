package com.hythecraft.hythemain.core.items.impl.misc;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.hythecraft.hythemain.core.inventory.inventories.misc.SatchelInventory;
import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.modifiers.ModifierBase;
import com.hythecraft.hythemain.core.items.types.RPGItem;
import com.hythecraft.hythemain.core.items.types.interfaces.InteractableItem;
import com.hythecraft.hythemain.core.items.util.ItemRarity;
import com.hythecraft.hythemain.core.items.util.ItemRequirement;
import com.hythecraft.hythemain.persistence.serialization.ItemStackSerializer;
import com.hythecraft.hythemain.util.ItemBuilder;

import de.tr7zw.changeme.nbtapi.NBTItem;

public class Satchel extends RPGItem implements InteractableItem{

	public static final String CONTENT_TAG = "SATCHEL_CONTENT";
	
	public Satchel() {
		super("SATCHEL", new ItemBuilder(Material.LEATHER).create(), "&7Satchel", CustomItemType.SATCHEL, "&7A small satchel that holds items");
	}

	@Override
	public void leftClick(PlayerInteractEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void rightClick(PlayerInteractEvent e) {
		if(e.getItem() == null || e.getItem().getType() == Material.AIR) return;
		if(!isApplicableItem(e.getItem())) return;
		NBTItem nbtItem = new NBTItem(e.getItem());
		ItemStack[] base64Inv = (ItemStack[]) new ItemStackSerializer().deserialize(nbtItem.getString(CONTENT_TAG) );
		new SatchelInventory(e.getPlayer(), base64Inv, nbtItem.getString(NBT_ITEM_TAG_UNIQUE)).openInventory(e.getPlayer());
	}

	@Override
	public ItemRarity rarity() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ItemRequirement> requirements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ModifierBase> modifiers() {
		// TODO Auto-generated method stub
		return null;
	}

}
