package com.hythecraft.hythemain.core.items.impl.misc;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;

import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.types.useable.ConsumableItem;
import com.hythecraft.hythemain.core.items.util.ItemRarity;
import com.hythecraft.hythemain.core.items.util.ItemRequirement;
import com.hythecraft.hythemain.util.ItemBuilder;

public class Undead_Flesh extends ConsumableItem{

	public Undead_Flesh() {
		super("UNDEAD_FLESH", new ItemBuilder(Material.ROTTEN_FLESH).create(), "&cUndead flesh", CustomItemType.UNDEAD_FLESH, "&7A foul stench radiates from this meat");
	}

	@Override
	public void leftClick(PlayerInteractEvent e) {

	}

	@Override
	public void rightClick(PlayerInteractEvent e) {

	}

	@Override
	public ItemRarity rarity() {
		return ItemRarity.Common;
	}

	@Override
	public List<ItemRequirement> requirements() {
		return null;
	}
	
}








