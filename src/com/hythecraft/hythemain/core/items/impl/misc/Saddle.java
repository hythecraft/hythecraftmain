package com.hythecraft.hythemain.core.items.impl.misc;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.spigotmc.event.entity.EntityDismountEvent;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.types.useable.ConsumableItem;
import com.hythecraft.hythemain.core.items.util.ItemRarity;
import com.hythecraft.hythemain.core.items.util.ItemRequirement;
import com.hythecraft.hythemain.util.ItemBuilder;

public class Saddle extends ConsumableItem{

	public Saddle() {
		super("SADDLE", new ItemBuilder(Material.SADDLE).create(), "&aSaddle", CustomItemType.SADDLE, "&7Right click to spawn Horse mount");
	}

	@Override
	public void leftClick(PlayerInteractEvent e) {

	}

	@Override
	public void rightClick(PlayerInteractEvent e) {
		if(!isApplicableItem(e.getItem())) return;
		Player player = e.getPlayer();
		Location loc = player.getLocation();
		Horse horse = (Horse) HytheCraft.getDefaultWorld().spawnEntity(loc, EntityType.HORSE);
		horse.setOwner(player);
		horse.setTamed(true);
		horse.getInventory().setSaddle(new ItemStack(Material.SADDLE, 1));
		horse.addPassenger(player);

	}
	
	@EventHandler
	public void dismount(EntityDismountEvent e) {
		if(e.getDismounted() instanceof Horse)
			e.getDismounted().remove();
	}

	@Override
	public ItemRarity rarity() {
		return ItemRarity.Uncommon;
	}

	@Override
	public List<ItemRequirement> requirements() {
		return null;
	}
	
}









