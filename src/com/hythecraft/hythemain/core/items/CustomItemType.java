package com.hythecraft.hythemain.core.items;

import java.util.ArrayList;
import java.util.List;

public enum CustomItemType {

	BUBYS_HELM,
	BUBYS_CHESTPLATE,
	BUBYS_LEGGINGS,
	BUBYS_BOOTIES,
	A_VERY_USEFUL_STICK,
	SOPHIAS_GOLDEN_NUGGET,
	SADDLE,
	LORD_NETIZEN_LXIX_EGGPLANT,
	FLOWER_HAT,
	UNDEAD_FLESH,
	TRAILBLAZING_BOOTS,
	SATCHEL;

	public static List<String> getAllList(){
		List<String> ret = new ArrayList<>();
		for(CustomItemType t : CustomItemType.values())
			ret.add(t.toString());
		return ret;
	}
	
	public static CustomItemType getById(String id)
	{
		return valueOf(id.toUpperCase().replace("-", "_"));
	}
	
	public String getId()
	{
		return name().toLowerCase().replace("_", "-");
	}
}
