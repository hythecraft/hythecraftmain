package com.hythecraft.hythemain.core.items.modifiers;

import com.hythecraft.hythemain.core.items.modifiers.combatmods.AttackDamage;
import com.hythecraft.hythemain.core.items.modifiers.staticmods.MaximumHealth;
import com.hythecraft.hythemain.core.items.modifiers.staticmods.Speed;

public enum Modifier {

	MAXIMUM_HEALTH("MAXIMUM_HEALTH", MaximumHealth.class),
	ADDED_ATTACK_DAMAGE("ADDED_ATTACK_DAMAGE", AttackDamage.class),
	SPEED("SPEED", Speed.class);
	
		
	public Class<? extends ModifierBase> base;
	public String name;
	
	Modifier(String name, Class<? extends ModifierBase> base){
		this.base = base;
		this.name = name;
	}

	public static Modifier getModifier(String name) {
		for(Modifier type : Modifier.values()) {
			if(type.name.toUpperCase().equals(name.toUpperCase()))
				return type;
		}
		return null;
	}
	
	public static Modifier getModifier(Class<? extends ModifierBase> base) {
		for(Modifier type : Modifier.values()) {
			if(type.base.equals(base))
				return type;
		}
		return null;
	}
}
