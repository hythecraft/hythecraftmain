package com.hythecraft.hythemain.core.items.modifiers;

import com.hythecraft.hythemain.core.combat.AttackInstance;

public abstract class CombatModifier extends ModifierBase{

	public CombatModifier(int tier, float[] value) {
		super(tier, value);
	}
	
	public CombatModifier(int tier) {
		super(tier);
	}

	public abstract void applyCombatMod(AttackInstance inst);
	
}
