package com.hythecraft.hythemain.core.items.modifiers;

public class ValueSet {
	public float min;
	public float max;
	public ValueSet(float min, float max) {
		this.min = min;
		this.max = max;
	}
}
