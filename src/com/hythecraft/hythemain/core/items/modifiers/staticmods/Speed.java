package com.hythecraft.hythemain.core.items.modifiers.staticmods;

import com.hythecraft.hythemain.core.items.modifiers.StaticModifier;
import com.hythecraft.hythemain.core.items.modifiers.ValueSet;
import com.hythecraft.hythemain.core.user.ProfileData;

public class Speed extends StaticModifier{

	static {
		round = true;
	}
	
	public void declareTierSets(){
		tierSet.put(1, new ValueSet(21, 25));
		tierSet.put(2, new ValueSet(16, 20));
		tierSet.put(3, new ValueSet(11, 15));
		tierSet.put(4, new ValueSet(6, 10));
		tierSet.put(5, new ValueSet(0, 5));
	}
	
	public Speed(int tier) {
		super(tier, null);
		initValue();
	}
	
	public Speed(int tier, float... value) {
		super(tier, value);
		initValue();
	}
	
	public void initValue() {
		super.name = "SPEED";
		super.description = "&f{0}% &7increased Walking Speed";
	}
	@Override
	public void applyMod(ProfileData player) {
		player.setSpeed( (value[0]/100f) *.2f + player.getSpeed());
	}
	
	
}












