package com.hythecraft.hythemain.core.items.modifiers.staticmods;

import com.hythecraft.hythemain.core.items.modifiers.StaticModifier;
import com.hythecraft.hythemain.core.items.modifiers.ValueSet;
import com.hythecraft.hythemain.core.user.ProfileData;

public class MaximumHealth extends StaticModifier{
	
	static {
		round = true;
	}
	
	public void declareTierSets(){
		tierSet.put(1, new ValueSet(70, 79));
		tierSet.put(2, new ValueSet(60, 69));
		tierSet.put(3, new ValueSet(50, 59));
		tierSet.put(4, new ValueSet(40, 49));
		tierSet.put(5, new ValueSet(30, 39));
	}
	
	public MaximumHealth(int tier) {
		super(tier, null);
		initValue();
	}
	
	public MaximumHealth(int tier, float... value) {
		super(tier, value);
		initValue();
	}
	
	public void initValue() {
		
		super.name = "MAXIMUM_HEALTH";
		super.description = "&f{0} &7increased Maximum Health";
	}
	@Override
	public void applyMod(ProfileData player) {
		player.setMaxHealth((int) (value[0] + player.getMaxHealth()));
	}
	
	
}












