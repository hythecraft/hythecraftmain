package com.hythecraft.hythemain.core.items.modifiers;

import java.util.HashMap;
import java.util.Map;

import com.hythecraft.hythemain.util.NumberUtil;

public class ModifierBase {
	
	public String name;
	public String description;
	public float[] value;
	public int tier;
	//If static all parents will share same values (DO NOT DO)
	public Map<Integer, ValueSet> tierSet = new HashMap<>();
	public static boolean round = false;

	public int valueCount = 1; //OVER RIDDEN IF THERE IS MORE THAN ONE VALUE ELSE STAY 1
	
	//Used for a new instance of as modifier (i.e rolling stats)
	public ModifierBase(int tier) {
		init(tier, null);
	}
	
	//Used to create already existing modifier
	public ModifierBase(int tier, float... value) {
		init(tier, value);
	}
	
	public Modifier getEnum() {
		return Modifier.getModifier(this.getClass());
	}

	public void declareTierSets() {
		
	}
	
	public void init(int tier, float... value) {
		
		declareTierSets();
		this.tier = tier;
		if(value != null)
			this.value = value;
		else
			rollValue(valueCount);
	}
	
	public String getDescription() {
		String s = description;
		for(int i = 0; i < value.length; i++)
		{
			if(value[i] % 1 != 0)
				s = s.replace("{" + i + "}", value[i] + "");
			else
				s = s.replace("{" + i + "}", (int)value[i] + "");
		}
		return s;
	}
	
	public void rollValue(int amount) {
		float[] values = new float[amount];
		for(int i = 0; i < amount; i++)
		{
			values[i] = (float)NumberUtil.random(tierSet.get(tier).min, tierSet.get(tier).max);
			if(round)
				values[i] = (int)NumberUtil.random((int)tierSet.get(tier).min, (int)tierSet.get(tier).max);
		}
		this.value = values;
	}
	
	@Override
	public String toString() {
		return name + "#" + tier + "#[" + splitList (value) + "]#";
	}

	
	public String splitList(float[] list) {
		String str = "";
		for(float f : list) {
			str += f+ ",";
		}
		str = str.substring(0, str.length()-1);
		return str;
	}
}
