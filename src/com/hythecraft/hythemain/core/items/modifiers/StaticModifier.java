package com.hythecraft.hythemain.core.items.modifiers;

import com.hythecraft.hythemain.core.user.ProfileData;

public abstract class StaticModifier extends ModifierBase{

	public StaticModifier(int tier, float[] value) {
		super(tier, value);
	}

	public StaticModifier(int tier) {
		super(tier);
	}
	
	public abstract void applyMod(ProfileData player);
}

