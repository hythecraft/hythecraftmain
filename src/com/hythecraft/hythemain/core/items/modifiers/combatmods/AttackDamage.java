package com.hythecraft.hythemain.core.items.modifiers.combatmods;

import com.hythecraft.hythemain.core.combat.AttackInstance;
import com.hythecraft.hythemain.core.items.modifiers.CombatModifier;
import com.hythecraft.hythemain.core.items.modifiers.ValueSet;
import com.hythecraft.hythemain.util.NumberUtil;

public class AttackDamage extends CombatModifier{
	
	static
	{
		round = true;
	}
	
	public void declareTierSets(){
		tierSet.put(1, new ValueSet(70, 79));
		tierSet.put(2, new ValueSet(60, 69));
		tierSet.put(3, new ValueSet(50, 59));
		tierSet.put(4, new ValueSet(40, 49));
		tierSet.put(5, new ValueSet(30, 39));
	}
	
	public AttackDamage(int tier) {
		super(tier, null);
		loadTierSet();
		initValue();
		
	}
	
	public AttackDamage(int tier, float... value) {
		super(tier, value);
		loadTierSet();
		super.valueCount = 2;
		initValue();
	}
	
	public void loadTierSet() {
	}
	
	public void initValue() {	
		super.name = "ADDED_ATTACK_DAMAGE";
		super.description = "&f{0}&7-&f{1} &7added Damage to Attacks";
		super.init(tier, null);
	}

	@Override
	public void applyCombatMod(AttackInstance inst) {
		inst.damage += NumberUtil.random(this.value[0], this.value[1]);
	}

	
}












