package com.hythecraft.hythemain.core.items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.inventory.ItemStack;

import com.hythecraft.hythemain.core.combat.AttackInstance;
import com.hythecraft.hythemain.core.items.modifiers.CombatModifier;
import com.hythecraft.hythemain.core.items.modifiers.Modifier;
import com.hythecraft.hythemain.core.items.modifiers.ModifierBase;
import com.hythecraft.hythemain.core.items.modifiers.StaticModifier;

import de.tr7zw.changeme.nbtapi.NBTItem;

public class ModifierManager {
	
	public final static String MODIFIER_TAG = "modifiers";

	public static List<String> getModLore(ItemStack item){
		List<String> ret = new ArrayList<>();
		if(getMods(item).length == 0)
			return ret;
		ret.add(" ");
		for(ModifierBase mod : getMods(item))
		{
			ret.add(mod.getDescription());
		}
		return ret;
	}
	
	public static ItemStack removeModifier(ItemStack item, Modifier mod) {
		List<ModifierBase> mods = Arrays.asList(getMods(item));
		Modifier[] existing = getModifierListEnum(item);
		for(Modifier m : existing)
			if(m != null)
				if(m.name.equals(mod.name))
					item = removeModifier(item, mod);
		return applyMods(item, (ModifierBase[]) mods.toArray());
	}
	
	public static ItemStack addModifier(ItemStack item, Modifier mod, int tier) {
		List<ModifierBase> mods = new ArrayList<ModifierBase>();
		mods.addAll(Arrays.asList(getMods(item)));
		try {
			Modifier[] existing = getModifierListEnum(item);
			for(Modifier m : existing)
				if(m != null)
					if(m.name.equals(mod.name))
						item = removeModifier(item, mod);
			mods.add( (ModifierBase)mod.base.getConstructor(int.class).newInstance(tier));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return applyMods(item, mods.toArray(new ModifierBase[mods.size()]));
	}
	
	public static ItemStack addModifier(ItemStack item, ModifierBase base) {
		List<ModifierBase> mods = new ArrayList<ModifierBase>();
		mods.addAll(Arrays.asList(getMods(item)));
		mods.add(base);
		return applyMods(item, mods.toArray(new ModifierBase[mods.size()]));
	}
	
	private static Modifier[] getModifierListEnum(ItemStack item) {
		List<Modifier> ret = new ArrayList<>();
		for(ModifierBase mod : getMods(item)) {
			ret.add(Modifier.getModifier(mod.getClass()));
		}
		if(ret.size() == 0) return new Modifier[0];
		return ret.toArray(new Modifier[ret.size()]);
	}
	
	public static StaticModifier[] getStaticMods(ItemStack item) {
		List<StaticModifier> staticList = new ArrayList<StaticModifier>();
		if(item != null && getMods(item) != null)
		for(ModifierBase mod : getMods(item))
		{
			if(mod instanceof StaticModifier)
			{
				staticList.add((StaticModifier)mod);
			}
		}
		return staticList.toArray(new StaticModifier[staticList.size()]);
	}
	
	public static CombatModifier[] getCombatMods(ItemStack item) {
		List<CombatModifier> combatList = new ArrayList<CombatModifier>();
		for(ModifierBase mod : getMods(item))
		{
			if(mod instanceof CombatModifier)
			{
				combatList.add((CombatModifier)mod);
			}
		}
		return combatList.toArray(new CombatModifier[combatList.size()]);
	}
	
	public static ItemStack applyMods(ItemStack item, ModifierBase[] mods) {
		if(mods == null) return item;
		if(item == null) return item;
		NBTItem nbtItem = new NBTItem(item);
		nbtItem.setString(MODIFIER_TAG, parseMods(mods));
		return nbtItem.getItem();
	}
	
	public static ModifierBase[] getMods(ItemStack item) {
		if(item == null) return new ModifierBase[0];
		NBTItem nbtItem = new NBTItem(item);
		if(!nbtItem.hasKey(MODIFIER_TAG)) return new ModifierBase[0];
		return parseModList(nbtItem.getString(MODIFIER_TAG));
	}
	
	private static String parseMods(ModifierBase[] mods) {
		String str = "";
		if(mods != null)
		for(ModifierBase mod : mods) {
			str += mod.toString() + "~";
		}
		return str;
	}
	
	private static ModifierBase[] parseModList(String modList) {
		String[] mods = modList.split("~");
		if(modList == null || modList == "") return new ModifierBase[0];

		List<ModifierBase> ret = new ArrayList<ModifierBase>();
		for(String str : mods)
		{
			try {
			String[] s = str.split("#");
			String name = s[0];
			int tier = Integer.parseInt(s[1]);
			String[] val = s[2].split(",");
			float[] value = new float[val.length];
			for(int i = 0; i < value.length; i++)
			{
				val[i] = val[i].replace("[", "").replace("]", "");
				value[i] = Float.parseFloat(val[i]);
			}
				
			
			ret.add(Modifier.getModifier(name).base.getDeclaredConstructor(new Class[] {int.class, float[].class}).newInstance(tier, value));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return ret.toArray(new ModifierBase[ret.size()]);
	}

	public static void applyCombatMod(AttackInstance attackInstance, CombatModifier[] combatMods) {
		for(CombatModifier mod : combatMods)
		{
			mod.applyCombatMod(attackInstance);
		}
		
	}
}
