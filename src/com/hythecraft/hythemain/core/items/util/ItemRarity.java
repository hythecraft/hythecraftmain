package com.hythecraft.hythemain.core.items.util;

import org.bukkit.ChatColor;

public enum ItemRarity {
	Very_Common("Very Common", ChatColor.WHITE),
	Common("Common", ChatColor.GRAY),
	Uncommon("Uncommon", ChatColor.GREEN),
	Rare("Rare", ChatColor.RED),
	Very_Rare("Very Rare", ChatColor.AQUA),
	Ultra_Rare("Ultra Rare", ChatColor.LIGHT_PURPLE);

	public ChatColor color;
	public String name;
	ItemRarity(String name, ChatColor color){
		this.color = color;
		this.name = name;
	}
	
	public static ItemRarity getRarity(String name) {
		try {
		for(ItemRarity rarity : ItemRarity.values()) {
			if(rarity.name.toLowerCase().equals(name.replace("_", " ").toLowerCase()))
				return rarity;
		}
		}
		catch(Exception e) {
			return null;
		}
		return null;
	}
}
