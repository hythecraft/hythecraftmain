package com.hythecraft.hythemain.core.items.util;

import com.hythecraft.hythemain.core.user.ProfileData;

public abstract class ItemRequirement {
	
	public abstract boolean fitReq(ProfileData user);
	public abstract String getReqString(ProfileData user);
}
