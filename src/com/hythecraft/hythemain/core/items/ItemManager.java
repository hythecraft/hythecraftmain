package com.hythecraft.hythemain.core.items;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.items.impl.armor.Bubys_Booties;
import com.hythecraft.hythemain.core.items.impl.armor.Bubys_Chestplate;
import com.hythecraft.hythemain.core.items.impl.armor.Bubys_Helm;
import com.hythecraft.hythemain.core.items.impl.armor.Bubys_Leggings;
import com.hythecraft.hythemain.core.items.impl.armor.FlowerHat;
import com.hythecraft.hythemain.core.items.impl.armor.Trailblazing_Boots;
import com.hythecraft.hythemain.core.items.impl.consumable.Lord_Netizen_LXIX_Eggplant;
import com.hythecraft.hythemain.core.items.impl.consumable.Sophias_Golden_Nugget;
import com.hythecraft.hythemain.core.items.impl.misc.Saddle;
import com.hythecraft.hythemain.core.items.impl.misc.Satchel;
import com.hythecraft.hythemain.core.items.impl.misc.Undead_Flesh;
import com.hythecraft.hythemain.core.items.impl.weapon.A_Very_Useful_Stick;
import com.hythecraft.hythemain.core.items.types.CustomItem;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.Colors;

import de.tr7zw.changeme.nbtapi.NBTItem;

public class ItemManager {

	private static Map<CustomItemType, CustomItem> customItems = new HashMap<>();
	
	public ItemManager() {
		registerItemHandler(new Bubys_Helm());
		registerItemHandler(new Bubys_Booties());
		registerItemHandler(new Bubys_Chestplate());
		registerItemHandler(new Bubys_Leggings());
		registerItemHandler(new A_Very_Useful_Stick());
		registerItemHandler(new Sophias_Golden_Nugget());
		registerItemHandler(new Saddle());
		registerItemHandler(new Lord_Netizen_LXIX_Eggplant());
		registerItemHandler(new FlowerHat());
		registerItemHandler(new Undead_Flesh());
		registerItemHandler(new Trailblazing_Boots());
		registerItemHandler(new Satchel());
	}
	
	public static CustomItem getCustomItemHandler(CustomItemType itemType)
	{
		return customItems.get(itemType);
	}
	
	private void registerItemHandler(CustomItem itemHandler)
	{
		customItems.put(itemHandler.getType(), itemHandler);
		Bukkit.getServer().getPluginManager().registerEvents(itemHandler, HytheCraft.getInstance());
	}
	
	public static boolean canUseItem(Player player, ItemStack itemStack) {
		return getCustomItemHandler(getCustomItemType(itemStack)).canUse(UserManager.getUser(player).getSelectedProfile());
	}
	
	public static CustomItemType getCustomItemType(ItemStack itemStack)
	{
		if(itemStack == null) return null;

		NBTItem item = new NBTItem(itemStack);
		
		if(!item.hasKey(CustomItem.NBT_ITEM_TAG_TYPE)) return null;

		Integer customItemTypeOrdinal = item.getInteger(CustomItem.NBT_ITEM_TAG_TYPE);

		if(customItemTypeOrdinal > CustomItemType.values().length - 1) return null;

		return CustomItemType.values()[customItemTypeOrdinal];
	}
	
	public ItemStack updateItem(ItemStack item, ProfileData user) {
		List<String> lore = ItemManager.getCustomItemHandler(ItemManager.getCustomItemType(item)).getItemLore(user, ModifierManager.getModLore(item));
		ItemMeta meta = item.getItemMeta();
		meta.setLore(Colors.parseColors(lore));
		item.setItemMeta(meta);
		return item;
	}
	
	public ItemStack getItemByUUID(Inventory inv, String uuid) {
		for(ItemStack item : inv.getContents())
		{
			if(item == null || item.getType() == Material.AIR) continue;
			
			NBTItem nbtItem = new NBTItem(item);
			if(nbtItem.getString(CustomItem.NBT_ITEM_TAG_UNIQUE).equals(uuid))
				return nbtItem.getItem();
		}
		return null;
	}
	
}















