package com.hythecraft.hythemain.core.items.requirements;

import com.hythecraft.hythemain.core.items.util.ItemRequirement;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.classtype.ClassType;

public class ClassRequirement extends ItemRequirement{

	private ClassType[] classes;
	
	public ClassRequirement(ClassType... classes) {
		this.classes = classes;
	}
	
	@Override
	public boolean fitReq(ProfileData user) {
		for(ClassType cl : classes)
			if(user.getClassType().equals(cl))
				return true;
		return false;
	}

	@Override
	public String getReqString(ProfileData user) {
		return fitReq(user) ? "&a✔ &7Class Requirement: &f" + getClassList() : "&c✖ &7Class Requirement: &f" + getClassList();
	}
	
	private String getClassList() {
		StringBuilder sb = new StringBuilder();
		for(ClassType cl : classes) {
			sb.append("&7" + cl.name + "&f, ");
		}
		return sb.toString().trim().replaceAll(",$", "");
	}

}
