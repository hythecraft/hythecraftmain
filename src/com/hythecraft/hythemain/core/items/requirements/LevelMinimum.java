package com.hythecraft.hythemain.core.items.requirements;

import com.hythecraft.hythemain.core.items.util.ItemRequirement;
import com.hythecraft.hythemain.core.user.ProfileData;

public class LevelMinimum extends ItemRequirement{

	private int min;
	
	public LevelMinimum(int min) {
		this.min = min;
	}
	
	@Override
	public boolean fitReq(ProfileData user) {
		return user.getLevel() >= min ? true : false;
	}

	@Override
	public String getReqString(ProfileData user) {
		return fitReq(user) ? "&a✔ &7Level Minimum: &f" + min: "&c✖ &7Level Minimum: &f" + min;
	}

}
