package com.hythecraft.hythemain.core.items.types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.ItemManager;
import com.hythecraft.hythemain.core.items.ModifierManager;
import com.hythecraft.hythemain.core.items.modifiers.ModifierBase;
import com.hythecraft.hythemain.core.items.types.interfaces.UntradableItem;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.Colors;
import com.hythecraft.hythemain.util.ItemBuilder;

import de.tr7zw.changeme.nbtapi.NBTItem;
import lombok.Getter;

public class CustomItem implements Listener{
	public final static String NBT_ITEM_TAG_TYPE = "HYTHE_CUSTOM_ITEM";
	public final static String NBT_ITEM_TAG_UNIQUE = "HYTHE_TAG_UNIQUE";
	
	@Getter CustomItemType itemCustomType;
	@Getter String ID;
	@Getter ItemStack item;
	@Getter String displayName;
	@Getter List<String> lore;
	
	public CustomItem(String ID, ItemStack item, String displayName, CustomItemType type, String... lore)
	{
		this.ID = ID;
		this.item = item;
		this.displayName = displayName;
		this.itemCustomType = type;
		this.lore = Arrays.asList(lore);
	}
	
	public List<String> getItemLore(ProfileData user, List<String> modLore)
	{
		List<String> ret = lore == null ? new ArrayList<>() : new ArrayList<String>(lore);
		return ret;
	}
	
	public boolean canUse(Player player) {
		return canUse(UserManager.getUser(player).getSelectedProfile());
	}
	
	
	public boolean canUse(ProfileData user) {
		if(!(this instanceof RPGItem)) return false;
		return ((RPGItem)this).meetRequirements(user);
	}
	
    public boolean isApplicableItem(ItemStack itemStack) {
        if (itemStack == null) return false;

        if (!getItemType().contains(itemStack.getType())) {
            return false;
        }

        CustomItemType type = ItemManager.getCustomItemType(itemStack);
        return type == this.itemCustomType;
    }
    
    public List<Material> getItemType() {
        return Collections.singletonList(item.getType());
    }   
    
    public CustomItemType getType() {
        return itemCustomType;
    }
	
    public ItemStack getNewStack(ProfileData data) {
        NBTItem nbtItem = new NBTItem(new ItemBuilder(item).create());

        nbtItem.setInteger(NBT_ITEM_TAG_TYPE, itemCustomType.ordinal());

        // stop custom items stacking
        nbtItem.setString(NBT_ITEM_TAG_UNIQUE, UUID.randomUUID().toString());

        ItemStack itemStack = nbtItem.getItem();
        ItemMeta itemMeta = itemStack.getItemMeta();

        if (itemMeta == null) itemMeta = Bukkit.getItemFactory().getItemMeta(itemStack.getType());

        itemMeta.setDisplayName(Colors.parseColors(displayName));

        itemStack.setItemMeta(itemMeta);
        if(this instanceof RPGItem)
        {
        	RPGItem rpg = (RPGItem)this;
        	if(rpg.modifiers() != null)
        	for(ModifierBase mod : rpg.modifiers())
        	{
        		itemStack = ModifierManager.addModifier(itemStack, mod);
        	}
        }
        if(data != null)
        	itemStack = HytheCraft.getInstance().getItemManager().updateItem(itemStack, data);
        return itemStack;
    }
    
    @SuppressWarnings("unused")
	private String translateAllowNull(String toTranslate) {
        return toTranslate != null ? ChatColor.translateAlternateColorCodes('&', toTranslate) : null;
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onAnvilClick(InventoryClickEvent event) {
        if (event.getClickedInventory() == null || event.getClickedInventory().getType() != InventoryType.ANVIL) return;
        if (event.getSlot() == 2 && isApplicableItem(event.getCurrentItem())) {
            event.setCancelled(true);
        }
    }

    // disable using custom items in enchanting table
    @EventHandler
    public void onItem(EnchantItemEvent event) {
        if (isApplicableItem(event.getItem())) event.setCancelled(true);
    }

    @EventHandler
    public void onPickup(EntityPickupItemEvent  event) {
    	if(!(event.getEntity() instanceof Player)) return;
    	ItemStack item = event.getItem().getItemStack().clone();
    	Player player = (Player)event.getEntity();
    	if(!isApplicableItem(item)) return;
    	HytheCraft.getInstance().getItemManager().updateItem(event.getItem().getItemStack(), UserManager.getUser(player).getSelectedProfile());
    	player.updateInventory();
    }
    
    @SuppressWarnings("deprecation")
	@EventHandler
    public void onPlayerItemDamage(PlayerItemDamageEvent event) {
        if (isApplicableItem(event.getItem())) {
            event.setCancelled(true);
            if (event.getItem().getDurability() > 0) event.getItem().setDurability((short) 0);
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onPrepareItemCraft(PrepareItemCraftEvent event) {
        for (ItemStack itemStack : event.getInventory().getMatrix()) {
            if (isApplicableItem(itemStack)) {
                event.getInventory().setResult(new ItemStack(Material.AIR));
                return;
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onItemInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock() != null
                && event.getClickedBlock().getType() == Material.SPAWNER && isApplicableItem(event.getItem())) {
            event.setUseItemInHand(Event.Result.DENY);
        }
    }

    @EventHandler
    public void dropEvent(PlayerDropItemEvent e) {
    	if(!this.isApplicableItem(e.getItemDrop().getItemStack())) return;
    	if(this instanceof UntradableItem) e.setCancelled(true);
    }
}
