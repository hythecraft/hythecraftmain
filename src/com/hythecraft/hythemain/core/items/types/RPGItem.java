package com.hythecraft.hythemain.core.items.types;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.modifiers.ModifierBase;
import com.hythecraft.hythemain.core.items.util.ItemRarity;
import com.hythecraft.hythemain.core.items.util.ItemRequirement;
import com.hythecraft.hythemain.core.user.ProfileData;

public abstract class RPGItem extends CustomItem {

	public RPGItem(String ID, ItemStack item, String displayName, CustomItemType type, String... lore) {
		super(ID, item, displayName, type, lore);
	}
	
	public abstract ItemRarity rarity();
	public abstract List<ItemRequirement> requirements();
	public abstract List<ModifierBase> modifiers();
	
	public boolean meetRequirements(ProfileData user) {
		if(requirements() == null) return true;
		for(ItemRequirement req : requirements())
		{
			if(!req.fitReq(user)) return false;
		}
		return true;
	}
	
	public List<String> getRequirementLore(ProfileData user){
		if(requirements() == null) return new ArrayList<>();
		List<String> ret = new ArrayList<>();
		ret.add(" ");
		requirements().forEach(r -> ret.add(r.getReqString(user)));
		return ret;
	}
	
	public List<String> getModifierLore(){
		if(modifiers() == null) return new ArrayList<>();
		List<String> ret = new ArrayList<>();
		ret.add(" ");
		modifiers().forEach(r -> ret.add(r.getDescription()));
		return ret;
		
	}
	
	@Override
	public List<String> getItemLore(ProfileData user, List<String> modLore)
	{
		modLore = modLore == null ? new ArrayList<>() : modLore;
		
		List<String> ret = lore == null ? new ArrayList<>() : new ArrayList<String>(lore);
		ret.addAll(getRequirementLore(user));
		ret.addAll(modLore);
		return ret;
	}
}
