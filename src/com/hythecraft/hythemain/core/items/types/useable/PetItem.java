package com.hythecraft.hythemain.core.items.types.useable;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.inventory.ItemStack;

import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.types.RPGItem;
import com.hythecraft.hythemain.core.pet.util.PetType;
import com.hythecraft.hythemain.core.user.ProfileData;

public abstract class PetItem extends RPGItem{

	public PetItem(String ID, ItemStack item, String displayName, CustomItemType type, String... lore) {
		super(ID, item, displayName, type, lore);
	}

	public abstract PetType petType();

	@Override
	public List<String> getItemLore(ProfileData user, List<String> modLore)
	{
		modLore = modLore == null ? new ArrayList<>() : modLore;
		
		List<String> ret = this.getLore() == null ? new ArrayList<>() : new ArrayList<String>(this.getLore());
		ret.add(" ");
		ret.addAll(getRequirementLore(user));
		ret.addAll(modLore);
		return ret;
	}
}
