package com.hythecraft.hythemain.core.items.types.useable;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.ModifierManager;
import com.hythecraft.hythemain.core.items.modifiers.ModifierBase;
import com.hythecraft.hythemain.core.items.types.RPGItem;
import com.hythecraft.hythemain.core.items.types.interfaces.InteractableItem;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.util.Colors;
import com.hythecraft.hythemain.util.ItemBuilder;

import de.tr7zw.changeme.nbtapi.NBTItem;

public abstract class ConsumableItem extends RPGItem implements InteractableItem{

	public ConsumableItem(String ID, ItemStack item, String displayName, CustomItemType type, String... lore) {
		super(ID, item, displayName, type, lore);
	}

	@Override
	public List<ModifierBase> modifiers() {
		return null;
	}
	
	
	//Allows items to stack
	@Override
	public ItemStack getNewStack(ProfileData data) {
        NBTItem nbtItem = new NBTItem(new ItemBuilder(this.getItem()).create());

        nbtItem.setInteger(NBT_ITEM_TAG_TYPE, this.getItemCustomType().ordinal());

        ItemStack itemStack = nbtItem.getItem();
        ItemMeta itemMeta = itemStack.getItemMeta();

        if (itemMeta == null) itemMeta = Bukkit.getItemFactory().getItemMeta(itemStack.getType());

        itemMeta.setDisplayName(Colors.parseColors(this.getDisplayName()));

        itemStack.setItemMeta(itemMeta);
        if(this instanceof RPGItem)
        {
        	RPGItem rpg = (RPGItem)this;
        	if(rpg.modifiers() != null)
        	for(ModifierBase mod : rpg.modifiers())
        	{
        		itemStack = ModifierManager.addModifier(itemStack, mod);
        	}
        }
        itemStack = HytheCraft.getInstance().getItemManager().updateItem(itemStack, data);
        return itemStack;
    }
	
}
