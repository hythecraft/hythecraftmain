package com.hythecraft.hythemain.core.entity.customentities;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R1.CraftWorld;

import com.hythecraft.hythemain.core.entity.HytheEntity;
import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.util.probability.ItemPool;

import net.minecraft.server.v1_16_R1.Entity;
import net.minecraft.server.v1_16_R1.EntityZombie;

public class Undead extends EntityZombie implements HytheEntity{

	private int level;
	private UUID uuid;
	
	public Undead(Location loc, int level) {
		super(((CraftWorld)loc.getWorld()).getHandle());
		this.level = level;
		this.uuid = UUID.randomUUID();
		this.spawn(loc);
		
	}
	
	@Override
	public Entity ent() {
		return this;
	}

	@Override
	public int level() {
		return this.level;
	}

	@Override
	public int healthFactor() {
		return 10;
	}

	@Override
	public int damageFactor() {
		return 1;
	}

	@Override
	public String displayName() {
		return "&7Undead (&aLvl. " + LEVEL_TAG + "&7)";
	}

	@Override
	public UUID uuid() {
		return this.uuid;
	}

	@Override
	public ItemPool itemPool() {
		return new ItemPool()
				.addItem(CustomItemType.UNDEAD_FLESH, 80)
				.addItem(CustomItemType.A_VERY_USEFUL_STICK, 20);
	}

}













