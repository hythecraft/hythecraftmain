package com.hythecraft.hythemain.core.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.craftbukkit.v1_16_R1.CraftWorld;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.persistence.PersistentDataType;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.entity.customentities.Undead;
import com.hythecraft.hythemain.util.Colors;
import com.hythecraft.hythemain.util.probability.ItemPool;

import net.minecraft.server.v1_16_R1.ChatMessage;
import net.minecraft.server.v1_16_R1.Entity;
import net.minecraft.server.v1_16_R1.EntityLiving;
import net.minecraft.server.v1_16_R1.GenericAttributes;

public interface HytheEntity extends Listener{

	public enum HytheEntityType{
		UNDEAD(Undead.class);
		
		public Class<? extends Entity> clazz;
		
		private HytheEntityType(Class<? extends Entity> clazz) {
			this.clazz = clazz;
		}
		
		public static HytheEntityType getEntity(String ID) {
			return HytheEntityType.valueOf(ID.toUpperCase());
		}
		
		public static List<String> getAllList(){
			List<String> ret = new ArrayList<>();
			for(HytheEntityType t : HytheEntityType.values())
				ret.add(t.toString());
			return ret;
		}
		
		public Entity spawn(Location loc, int level) {
			try {
				Entity e = this.clazz.getDeclaredConstructor(Location.class, int.class).newInstance(loc, level);
				return e;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	
	public static final String LEVEL_TAG = "$LEVEL";
	
	public Entity ent();
	public int level();
	public int healthFactor();
	public int damageFactor();
	public String displayName();
	public UUID uuid();
	public ItemPool itemPool();
	
	public default void spawn(Location loc) {
		HytheCraft.getInstance().registerListener(this);
		ent().setPosition(loc.getX(), loc.getY(), loc.getZ());
		((CraftWorld)loc.getWorld()).getHandle().addEntity(ent());	
		ent().setCustomName(new ChatMessage(Colors.parseColors(displayName().replace(LEVEL_TAG, level() + "") )));
		ent().setCustomNameVisible(true);
		
		((EntityLiving)ent()).getAttributeInstance(GenericAttributes.MAX_HEALTH).setValue(level() * healthFactor());
		((EntityLiving)ent()).setHealth(level() * healthFactor()); 
		
		((EntityLiving)ent()).getAttributeInstance(GenericAttributes.ATTACK_DAMAGE).setValue(level() * damageFactor());
		
		ent().getBukkitEntity().getPersistentDataContainer().set(getKey("UUID"), PersistentDataType.STRING, uuid().toString());

		new EntityHealthBar(ent());
	}

	@EventHandler
	public default void onDeath(EntityDeathEvent e) {
		if(e.getEntity().getPersistentDataContainer().get(getKey("UUID"), PersistentDataType.STRING).equals(uuid().toString()))
		{
			e.setDroppedExp(0);
			e.getDrops().clear();
			e.getDrops().addAll(itemPool().generate());
		}
	}
	
	public static NamespacedKey getKey(String key) {
		return new NamespacedKey(HytheCraft.getInstance(), key);
	}
}




















