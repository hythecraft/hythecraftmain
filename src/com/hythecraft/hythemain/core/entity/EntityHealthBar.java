package com.hythecraft.hythemain.core.entity;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.listener.events.updates.UpdateTickEvent;
import com.hythecraft.hythemain.util.Colors;

import net.minecraft.server.v1_16_R1.Entity;

public class EntityHealthBar implements Listener{

	private final static double offsetY = .25;
	
	Entity ent;
	org.bukkit.entity.Entity armorStand;

	public EntityHealthBar(Entity ent) {
		this.ent = ent;
		Location loc = ent.getBukkitEntity().getLocation();
		
		armorStand = loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
		
		ArmorStand stand = (ArmorStand)armorStand;
		armorStand.setInvulnerable(true);
		stand.setCustomNameVisible(true);
		stand.setVisible(false);
		stand.setArms(false);
		HytheCraft.getInstance().registerListener(this);
	}
	
	@EventHandler
	public void deathEvent(EntityDeathEvent e)
	{
		if(e.getEntity().equals((org.bukkit.entity.Entity)ent.getBukkitEntity()))
		armorStand.remove();
	}

	@EventHandler
	public void execute(UpdateTickEvent e) {
		try {
			LivingEntity entity = (LivingEntity) ent.getBukkitEntity();
			armorStand.teleport(ent.getBukkitEntity().getLocation().clone().add(new Location(entity.getWorld(),0,offsetY,0)));
			armorStand.setCustomName(Colors.parseColors( (int)(entity).getHealth() + " &c❤"));
		}catch(Exception excp) {
			if(armorStand != null)
				armorStand.remove();
		}
	}
	
}
