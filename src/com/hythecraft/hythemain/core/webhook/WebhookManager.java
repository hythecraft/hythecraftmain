package com.hythecraft.hythemain.core.webhook;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.user.UserData;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.persistence.database.util.RedisPacket;
import com.hythecraft.hythemain.persistence.database.util.RedisPublish;
import com.hythecraft.hythemain.util.Colors;

public class WebhookManager {

	
	public void sendHookRequest(Player player, String googleID) {
		UserData user = UserManager.getUser(player);
		if(user.getLastLinkReq() != null && user.getLastLinkReq().equals(googleID)) return;
		user.setLastLinkReq(googleID);
		player.sendMessage(Colors.parseColors("&7A Profile with the id " + googleID + " is attempting to link to your minecraft account"));
		player.sendMessage(Colors.parseColors("&7type &a/webhook accept &7to accept link"));
	}
	
	public void acceptReq(Player player) {
		UserData user = UserManager.getUser(player);
		if(user.getLastLinkReq() == null || user.getLastLinkReq().isEmpty()) return;
		
		new RedisPublish(RedisPacket.PLUGIN_TO_SITE_ACCEPT_LINK, user.getOnlineUUID(), user.getLastLinkReq());
		user.setLastLinkReq(null);
	}
}
