package com.hythecraft.hythemain.core.pet.util;

import lombok.Getter;

public class PetBase {
	@Getter private String ID;
	@Getter private String displayName;
	@Getter private String description;
	
	public PetBase(String ID, String displayName, String description) {
		this.ID = ID;
		this.displayName = displayName;
		this.description = description;
	}
}
