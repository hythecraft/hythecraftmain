package com.hythecraft.hythemain.core.pet.util;

import com.hythecraft.hythemain.core.pet.TestPet;

import lombok.Getter;

public enum PetType {

	TEST(new TestPet());
	
	@Getter private String ID;
	@Getter private PetBase base;
	
	PetType(PetBase base) {
		this.base = base;
		this.ID = base.getID();
	}
	
	public static PetType getPet(String ID) {
		for(PetType t : PetType.values())
			if(t.getID().equalsIgnoreCase(ID))
				return t;
		return null;
	}
	
}
