package com.hythecraft.hythemain.persistence.serialization;

import com.hythecraft.hythemain.core.pet.util.PetType;

//Base64 Encoder
public class PetTypeSerializer extends VariableSerializer{
	@Override
	public String serialize(Object obj) {
		if(!(obj instanceof PetType)) return null;
		return ((PetType)obj).getID();
		
	}

	@Override
	public Object deserialize(String str) {
		return PetType.getPet(str);
	}
    

}