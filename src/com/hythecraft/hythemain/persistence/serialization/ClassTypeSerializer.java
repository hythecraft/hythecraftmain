package com.hythecraft.hythemain.persistence.serialization;

import com.hythecraft.hythemain.core.user.classtype.ClassType;

public class ClassTypeSerializer extends VariableSerializer{

	@Override
	public String serialize(Object obj) {
		if(!(obj instanceof ClassType)) return null;
		ClassType type = (ClassType)(obj);
		return type.name;
	}

	@Override
	public Object deserialize(String str) {
		return ClassType.getTypeByName(str);
	}

}
