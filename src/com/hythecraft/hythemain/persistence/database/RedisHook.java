package com.hythecraft.hythemain.persistence.database;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.persistence.database.util.RedisPacket;
import com.hythecraft.hythemain.persistence.database.util.RedisPublish;

import lombok.Getter;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public class RedisHook {
	@Getter private Jedis jedis;
	
	public RedisHook() {
		jedis = new Jedis(RedisPacket.JEDIS_IP, RedisPacket.JEDIS_PORT);	
		
		Thread thread = new Thread() {
			public void run() {
				reciever();
			}
		};
		thread.start();
	}
	
	public void reciever() {
	    JedisPubSub jedisPubSub = new JedisPubSub() {
	        @Override
	        public void onPMessage(String pattern, String channel, String message) {
	            RedisPacket packet = RedisPacket.asPacket(message);
	            if(packet == null) return;
	            
	            Map<String, String> args = packet.getArgsIncoming(message);
	            
	            switch(packet) {
				case SITE_TO_PLUGIN_PLAYER_ONLINE:
					for(Player pl : Bukkit.getOnlinePlayers())
					{
						String plUUID = UserManager.getUser(pl).getOnlineUUID();
						//Have to use mojang api to get uuid since pl.getUniqueId() will return offline id
						
						
						String googleID = args.get("googleID");
						if(plUUID.equals(args.get("uuid"))) {
							new RedisPublish(RedisPacket.PLUGIN_TO_SITE_PLAYER_ONLINE, plUUID, googleID);
						}
					}
					break;
				case SITE_TO_PLUGIN_REQUEST_LINK:
					boolean contin = false;
					for(Player pl : Bukkit.getOnlinePlayers())
					{
						if(pl.getName().equals(args.get("uuid")))
							contin = true;
					}
					if(!contin) return;
					

					String googleID = args.get("googleID");
					String plUUID = args.get("uuid");
					
					HytheCraft.getInstance().getWebhookManager().sendHookRequest(Bukkit.getPlayer(plUUID), googleID);
					
					break;
				default:
					return;
	            	
	            }
	            
	        }
	        
	        
	    };
	    
	    jedis.psubscribe(jedisPubSub, RedisPacket.PRIMARY_CHANNEL);
	}
}
