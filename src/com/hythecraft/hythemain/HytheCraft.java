package com.hythecraft.hythemain;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.hythecraft.hythemain.command.CommandManager;
import com.hythecraft.hythemain.core.area.mob.util.MobAreaManager;
import com.hythecraft.hythemain.core.area.town.util.TownManager;
import com.hythecraft.hythemain.core.auctionhouse.AuctionHouseManager;
import com.hythecraft.hythemain.core.characters.CharacterManager;
import com.hythecraft.hythemain.core.chat.listeners.ListenerAsyncPlayerChatEvent;
import com.hythecraft.hythemain.core.chat.listeners.ListenerPlayerCommandProcessEvent;
import com.hythecraft.hythemain.core.chat.privatemessage.PrivateMessageManager;
import com.hythecraft.hythemain.core.dialogue.util.DialogueManager;
import com.hythecraft.hythemain.core.inventory.util.InventoryManager;
import com.hythecraft.hythemain.core.items.ItemManager;
import com.hythecraft.hythemain.core.listener.events.updates.UpdateSecondEvent;
import com.hythecraft.hythemain.core.listener.events.updates.UpdateTickEvent;
import com.hythecraft.hythemain.core.listener.listeners.area.ListenerTownEnterEvent;
import com.hythecraft.hythemain.core.listener.listeners.area.ListenerTownExitEvent;
import com.hythecraft.hythemain.core.listener.listeners.block.ListenerBlockBurnEvent;
import com.hythecraft.hythemain.core.listener.listeners.block.ListenerBlockExplodeEvent;
import com.hythecraft.hythemain.core.listener.listeners.block.ListenerBlockSpreadEvent;
import com.hythecraft.hythemain.core.listener.listeners.entity.ListenerCreatureSpawnEvent;
import com.hythecraft.hythemain.core.listener.listeners.entity.ListenerEntityCombustEvent;
import com.hythecraft.hythemain.core.listener.listeners.entity.ListenerEntityDamageByEntityEvent;
import com.hythecraft.hythemain.core.listener.listeners.entity.ListenerEntityDamageEvent;
import com.hythecraft.hythemain.core.listener.listeners.entity.ListenerEntityExplodeEvent;
import com.hythecraft.hythemain.core.listener.listeners.entity.ListenerEntityPickupItemEvent;
import com.hythecraft.hythemain.core.listener.listeners.entity.ListenerEntityRegainHealthEvent;
import com.hythecraft.hythemain.core.listener.listeners.entity.ListenerEntityShootBowEvent;
import com.hythecraft.hythemain.core.listener.listeners.misc.ListenerBlockGrowEvent;
import com.hythecraft.hythemain.core.listener.listeners.misc.ListenerProjectileHitEvent;
import com.hythecraft.hythemain.core.listener.listeners.player.ListenerPlayerArmorChangeEvent;
import com.hythecraft.hythemain.core.listener.listeners.player.ListenerPlayerInteractEntityEvent;
import com.hythecraft.hythemain.core.listener.listeners.player.ListenerPlayerJoinEvent;
import com.hythecraft.hythemain.core.listener.listeners.player.ListenerPlayerMoveEvent;
import com.hythecraft.hythemain.core.permission.PlayerPermissionManager;
import com.hythecraft.hythemain.core.quests.util.QuestManager;
import com.hythecraft.hythemain.core.tablist.TabListManager;
import com.hythecraft.hythemain.core.trading.TradeManager;
import com.hythecraft.hythemain.core.user.PlayerUnloadedCheck;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.core.webhook.WebhookManager;
import com.hythecraft.hythemain.core.world.weather.WeatherManager;
import com.hythecraft.hythemain.persistence.database.MongoHook;
import com.hythecraft.hythemain.persistence.database.RedisHook;
import com.hythecraft.hythemain.util.annotations.Manager;

import lombok.Getter;

public class HytheCraft extends JavaPlugin{
	
	private static HytheCraft instance;
	@Getter private static UUID sessionSecret;
	
	@Getter private static Location spawnLoc;
	@Getter private static World defaultWorld;
	
	@Getter @Manager private ItemManager itemManager;
	@Getter @Manager private CommandManager commandManager;
	@Getter @Manager private PrivateMessageManager privetMessageManager;
	@Getter @Manager private QuestManager questManager;
	@Getter @Manager private DialogueManager dialogueManager;
	@Getter @Manager private TownManager townManager;
	@Getter @Manager private MobAreaManager mobAreaManager;
	@Getter @Manager private AuctionHouseManager auctionHouseManager;
	@Getter @Manager private WeatherManager weatherManager;
	@Getter @Manager private TabListManager tablistManager;
	@Getter @Manager private TradeManager tradeManager;
	@Getter @Manager private CharacterManager characterManager;
	@Getter @Manager private InventoryManager inventoryManager;
	
	//permission
	@Getter @Manager private PlayerPermissionManager playerPermissionManager;
	
	//webhook
	@Getter @Manager private WebhookManager webhookManager;
	
	//db
	@Getter @Manager private MongoHook mongoHook;
	@Getter @Manager private RedisHook redisHook;
		
	@Override
	public void onEnable(){
		instance = this;
		try {
			setupManagers();
			setupListeners();
			setupTimers();
			generateSecret();
		}catch(Exception e) {
			e.printStackTrace();
		}
		defaultWorld = Bukkit.getWorld("Terrain");
		spawnLoc = new Location(defaultWorld, -63, 64, 3);
		
		Bukkit.clearRecipes();
	}
	
	@Override
	public void onDisable() {
		UserManager.onDisable();
	}
	
	public static HytheCraft getInstance() {	
		return instance;
	}
	
	private void setupTimers() {
		new BukkitRunnable() {
			@Override
			public void run() {
				Bukkit.getPluginManager().callEvent(new UpdateSecondEvent());
			}
			
		}.runTaskTimer(this, 0, 20);
		new BukkitRunnable() {
			@Override
			public void run() {
				Bukkit.getPluginManager().callEvent(new UpdateTickEvent());
			}
			
		}.runTaskTimer(this, 0, 1);
	}
	
    private void setupManagers() throws Exception{
        for(Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);

            if(field.getAnnotation(Manager.class) == null) continue;
            Constructor<?> constructor = field.getType().getDeclaredConstructor();
            field.set(this, constructor.newInstance());
        }
    }

    private void setupListeners() {
    	registerListener(
    			//Chat
    			new ListenerAsyncPlayerChatEvent(),
    			new ListenerPlayerCommandProcessEvent(),
    			
    			//Movement
    			new ListenerPlayerMoveEvent(),
    			
    			//Entity
    			new ListenerEntityDamageByEntityEvent(),
    			new ListenerEntityDamageEvent(),
    			new ListenerEntityCombustEvent(),
    			new ListenerEntityPickupItemEvent(),
    			new ListenerEntityRegainHealthEvent(),
    			new ListenerEntityShootBowEvent(),
    			new ListenerCreatureSpawnEvent(),
    			
    			//Player Unloaded Check
    			new PlayerUnloadedCheck(),
    			new ListenerPlayerJoinEvent(),
    			
    			
    			new ListenerTownEnterEvent(),
    			new ListenerTownExitEvent(),
    			
    			new ListenerPlayerArmorChangeEvent(),
    			new ListenerEntityExplodeEvent(),
    			
    			new ListenerBlockExplodeEvent(),
    			new ListenerBlockSpreadEvent(),
    			new ListenerBlockBurnEvent(),
    			new ListenerBlockGrowEvent(),
    			
    			new ListenerProjectileHitEvent(),
    			new ListenerPlayerInteractEntityEvent()
    			);
    }
    
	public void registerListener(Listener... listeners) {
		PluginManager manager = getServer().getPluginManager();
		for(Listener l : listeners) {
			manager.registerEvents(l, this);
		}
	}
	
	public void unregisterListener(Listener... listeners) {
		for(Listener l : listeners)
		{
			HandlerList.unregisterAll(l);	
		}
	}
	
	public void generateSecret() {
		sessionSecret = UUID.randomUUID();
	}
}
