package com.hythecraft.hythemain.test;

import lombok.Getter;

public class ObjectAnimationSequence {
	@Getter private AnimationFrame[] frames;
}
