package com.hythecraft.hythemain.test;

import lombok.Getter;

public class EntityAnimationSequence {
	@Getter private ObjectAnimationSequence[] animationSequence;
}
