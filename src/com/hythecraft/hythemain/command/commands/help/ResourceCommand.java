package com.hythecraft.hythemain.command.commands.help;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.user.UserManager;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("resourcepack|resource|texture|texturepack")
public class ResourceCommand extends BaseCommand{

	@Default
	public void execute(final CommandSender sender) {
		UserManager.loadResourcePack((Player)sender);
	}

}
