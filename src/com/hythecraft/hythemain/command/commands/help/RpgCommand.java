package com.hythecraft.hythemain.command.commands.help;

import org.bukkit.command.CommandSender;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.util.Colors;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import net.md_5.bungee.api.ChatColor;

@CommandAlias("rpg")
public class RpgCommand extends BaseCommand{

	@Default
	public void execute(final CommandSender sender) {
		sender.sendMessage(Colors.parseColors("&7-------------------"));
		sender.sendMessage(Colors.parseColors("&7HytheCraft RPG"));
		sender.sendMessage(Colors.parseColors("&7Lead Developer: "+ ChatColor.AQUA+"Buby"));
		sender.sendMessage(Colors.parseColors("&7Version: " + ChatColor.AQUA + HytheCraft.getInstance().getDescription().getVersion()));
		sender.sendMessage(Colors.parseColors("&7https://www.hythecraft.com"));
		sender.sendMessage(Colors.parseColors("&7-------------------"));
	}

}
