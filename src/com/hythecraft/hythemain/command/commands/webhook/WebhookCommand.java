package com.hythecraft.hythemain.command.commands.webhook;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.HytheCraft;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("webhook")
public class WebhookCommand extends BaseCommand{
	
	@Subcommand("accept")
	public void execute(final CommandSender sender) {
		Player player = (Player)sender;
		HytheCraft.getInstance().getWebhookManager().acceptReq(player);
		
	}

}
