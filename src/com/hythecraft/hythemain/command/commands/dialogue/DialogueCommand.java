package com.hythecraft.hythemain.command.commands.dialogue;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.dialogue.util.dialoguetypes.DialogueBase;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;

@CommandAlias("dialogue")
public class DialogueCommand extends BaseCommand{
	
	@Default
	@CommandCompletion("@dialogues")
	public void execute(final CommandSender sender, DialogueBase base)
	{
		Player player = (Player) sender;
		HytheCraft.getInstance().getDialogueManager().startDialogue(base, player);
	}

}
	
	
	
	
	
	
	
	
	