package com.hythecraft.hythemain.command.commands.user;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.user.UserManager;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("character|char")
public class CharacterCommand extends BaseCommand{

	@Default
	public void execute(final CommandSender sender) {
		Player player = (Player)sender;
		if(UserManager.getUser(player).getSelectedProfile() != null)
			UserManager.getUser(player).getSelectedProfile().saveProfile();
		UserManager.getUser(player).setSelectedProfile(null);
		player.getInventory().clear();
	}

}
