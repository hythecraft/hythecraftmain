package com.hythecraft.hythemain.command.commands.items;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.items.ItemManager;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;

@CommandAlias("give")
@CommandPermission("hythecraft.admin")
public class GiveItemCommand extends BaseCommand{

	@Default
	@CommandCompletion("@hythe-items")
	public void execute(CommandSender sender, CustomItemType type, Player player, @Default("1") Integer amount) {
		
		ProfileData data = UserManager.getUser(player).getSelectedProfile();
		ItemStack item = ItemManager.getCustomItemHandler(type).getNewStack(data).add(amount - 1);
		player.getInventory().addItem(item);
	}

}
