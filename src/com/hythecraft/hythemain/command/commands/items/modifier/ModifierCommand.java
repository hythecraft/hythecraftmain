package com.hythecraft.hythemain.command.commands.items.modifier;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.items.ModifierManager;
import com.hythecraft.hythemain.core.items.modifiers.Modifier;
import com.hythecraft.hythemain.core.user.UserManager;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;

@CommandAlias("modifier,mod")
@CommandPermission("hythecraft.admin")
public class ModifierCommand extends BaseCommand{

	@Default
	public void execute(CommandSender sender, Modifier mod, Integer tier) {
		Player player = (Player) sender;
		ItemStack item = player.getInventory().getItemInMainHand();
		item = HytheCraft.getInstance().getItemManager().updateItem(ModifierManager.addModifier(item, mod, tier), UserManager.getUser(player).getSelectedProfile());
		player.getInventory().setItemInMainHand(item);
	}

}
