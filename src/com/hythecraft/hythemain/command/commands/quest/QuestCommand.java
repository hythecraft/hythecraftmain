package com.hythecraft.hythemain.command.commands.quest;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.inventory.inventories.quest.QuestInventory;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.bukkit.contexts.OnlinePlayer;

@CommandAlias("quest|q|quests")
public class QuestCommand extends BaseCommand{
	
	@Default
	public void execute(final CommandSender sender) {
		Player player = (Player)sender;
		new QuestInventory(player).openInventory(player);
	}
	
	@Subcommand("start")
	public void startQuest(final CommandSender sender, String questID, OnlinePlayer player)
	{
		HytheCraft.getInstance().getQuestManager().startQuest(questID.toUpperCase(), player.getPlayer());
	}

}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	