package com.hythecraft.hythemain.command.commands.entity;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.core.entity.HytheEntity.HytheEntityType;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;

@CommandAlias("spawn")
@CommandPermission("hythecraft.admin")
public class SpawnCommand extends BaseCommand{

	@Default
	@CommandCompletion("@hythe-entity")
	public void execute(final CommandSender sender, HytheEntityType type, @Default("1") Integer level, @Default("1") Integer amount)
	{
		for(int i = 0; i < amount; i++)
			type.spawn(((Player)sender).getLocation(), level);
	}
}
