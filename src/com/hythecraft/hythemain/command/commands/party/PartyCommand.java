package com.hythecraft.hythemain.command.commands.party;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.config.Language;
import com.hythecraft.hythemain.core.party.PartyObject;
import com.hythecraft.hythemain.core.party.util.PartyRemoveReason;
import com.hythecraft.hythemain.core.user.ProfileData;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.Colors;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.bukkit.contexts.OnlinePlayer;

@CommandAlias("party")
public class PartyCommand extends BaseCommand{

	@Default
	public void execute(final CommandSender sender) {		
		Player player = (Player)sender;
		ProfileData profile = UserManager.getUser(player).getSelectedProfile();
		PartyObject party = profile.getCurrentParty();
		
		if(party == null) {
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.NOT_IN_PARTY));
		}
		else {
			player.sendMessage(Colors.parseColors("&aParty Leader: " + Bukkit.getPlayer(party.getLeader()).getDisplayName()));
			player.sendMessage(Colors.parseColors(""));
		}
	}
	
	@Subcommand("create")
	public void create(final CommandSender sender) {
		Player player = (Player)sender;
		ProfileData profile = UserManager.getUser(player).getSelectedProfile();
		PartyObject party = profile.getCurrentParty();
		
		if(party != null)	
		{
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.ALREADY_IN_PARTY));
			return;
		}
		
		PartyObject newParty = new PartyObject(player);
		profile.setCurrentParty(newParty);
		player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.PARTY_CREATED_SUCCESSFULLY));
	}
	
	@Subcommand("confirm|accept")
	public void confirm(final CommandSender sender) {
		Player player = (Player)sender;
		ProfileData profile = UserManager.getUser(player).getSelectedProfile();
		PartyObject party = profile.getCurrentParty();
		
		if(profile.getLastPartyRequest() == null)
		{
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.NO_PARTY_REQUEST));

			return;
		}
		if(!profile.getLastPartyRequest().addPlayer(player)) {
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.PARTY_FULL));
			return;
		}
		if(party != null) {
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.ALREADY_IN_PARTY));
			return;
		}
		profile.setLastPartyRequest(null);
	}
	
	@Subcommand("decline")
	public void decline(final CommandSender sender) {
		Player player = (Player)sender;
		ProfileData profile = UserManager.getUser(player).getSelectedProfile();
		
		if(profile.getLastPartyRequest() == null)
		{
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.NO_PARTY_REQUEST));
			return;
		}
		
		profile.setLastPartyRequest(null);
	}
	
	@Subcommand("leave")
	public void leave(final CommandSender sender) {
		Player player = (Player)sender;
		ProfileData profile = UserManager.getUser(player).getSelectedProfile();
		PartyObject party = profile.getCurrentParty();
		
		if(party == null)
		{
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.NOT_IN_PARTY));
			return;
		}
		if(party.getLeader().equals(profile.getPlayer().getUniqueId()))
		{
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.PARTY_CANT_LEAVE_AS_LEADER));
			return;
		}
		party.removePlayer(player, PartyRemoveReason.LEAVE);
	}
	
	@Subcommand("invite")
	public void invite(final CommandSender sender, OnlinePlayer target) {
		Player player = (Player)sender;
		ProfileData profile = UserManager.getUser(player).getSelectedProfile();
		PartyObject party = profile.getCurrentParty();
		
		if(party == null) {
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.NOT_IN_PARTY));
			return;
		}
		if(!party.getLeader().equals(player.getUniqueId()))
		{
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.NOT_PARTY_LEADER));
			return;
		}
		if(target == null) {
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.NO_SUCH_PLAYER_NAME));
			return;
		}
		if(UserManager.getUser(target.getPlayer()).getSelectedProfile() == null)
		{
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.CANNOT_INVITE_PLAYER));
			return;
		}
		if(target.equals(player))
		{
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.CANT_INVITE_SELF));
			return;
		}
		if(UserManager.getUser(target.getPlayer()).getSelectedProfile().getCurrentParty() != null)
		{
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.PLAYER_ALREADY_IN_PARTY));
			return;
		}
		if(!UserManager.getUser(target.getPlayer()).getSelectedProfile().isAcceptPartyInvites())
		{
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.PLAYER_PARTY_INVITE_DISABLED));
		}
		{
			party.invitePlayer(target.getPlayer());
		}
	}
	
	@Subcommand("disband")
	public void disband(final CommandSender sender) {
		Player player = (Player)sender;
		ProfileData profile = UserManager.getUser(player).getSelectedProfile();
		PartyObject party = profile.getCurrentParty();
		
		if(party == null) {
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.NOT_IN_PARTY));
			return;
		}
		if(!party.getLeader().equals(player.getUniqueId()))
		{
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.NOT_PARTY_LEADER));
			return;
		}
		party.disband();
	}
	
	@Subcommand("kick")
	public void kick(final CommandSender sender, OnlinePlayer target) {
		Player player = (Player)sender;
		ProfileData profile = UserManager.getUser(player).getSelectedProfile();
		PartyObject party = profile.getCurrentParty();
		
		if(party == null) {
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.NOT_IN_PARTY));
			return;	
		}
		if(!party.getLeader().equals(player.getUniqueId()))
		{
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.NOT_PARTY_LEADER));
			return;
		}
		if(target.equals(player))
		{
			player.sendMessage(Colors.parseColors(Language.PARTY_PREFIX + Language.PARTY_CANT_KICK_SELF));
			return;
		}
		
		{
			party.removePlayer(target.getPlayer(), PartyRemoveReason.KICK);
		}
	}
	
	@Subcommand("promote")
	public void promote(final CommandSender sender, OnlinePlayer player) {
	}
	
	@Subcommand("help,?")
	public void help(final CommandSender sender) {
		
	}


}
