package com.hythecraft.hythemain.command.commands.permission;

import org.bukkit.command.CommandSender;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.permission.PermissionGroup;
import com.hythecraft.hythemain.core.user.UserManager;
import com.hythecraft.hythemain.util.Colors;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.HelpCommand;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.bukkit.contexts.OnlinePlayer;

@CommandAlias("permission|perm")
@CommandPermission("hythecraft.admin")
public class PermissionCommand extends BaseCommand{

	@HelpCommand
	public void help(final CommandSender sender) {
		
	}
	
	@Subcommand("group")
	public class group extends BaseCommand{
		
		@Subcommand("set")
		@CommandCompletion("@players @hythe-perm-groups")
		public void set(final CommandSender sender, OnlinePlayer target, PermissionGroup group)
		{
			HytheCraft.getInstance().getPlayerPermissionManager().setPlayerGroup(target.player, group);
			sender.sendMessage(Colors.parseColors(target.player.getDisplayName() + "&7's group set to " + UserManager.getUser(target.player).getGroup().getGroupID()));
			
		}
	}
	

}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	