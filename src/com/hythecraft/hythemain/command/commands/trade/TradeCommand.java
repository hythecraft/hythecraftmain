package com.hythecraft.hythemain.command.commands.trade;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.HytheCraft;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Subcommand;

@CommandAlias("trade")
public class TradeCommand extends BaseCommand{
	
	@Subcommand("accept")
	public void accept(final CommandSender sender) {
		Player player = (Player) sender;
		HytheCraft.getInstance().getTradeManager().acceptTrade(player);

	}	
	
	@Subcommand("decline")
	public void decline(final CommandSender sender) {
		Player player = (Player) sender;
		HytheCraft.getInstance().getTradeManager().declineTrade(player);

	}

}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	