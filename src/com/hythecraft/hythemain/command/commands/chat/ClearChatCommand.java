package com.hythecraft.hythemain.command.commands.chat;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;

@CommandAlias("clearchat")
@CommandPermission("hythecraft.admin")
public class ClearChatCommand extends BaseCommand{
	private static final int amount = 100;
	
	@Default
	public void execute(final CommandSender sender) {
		for(int i = 0; i < amount; i++) {
			Bukkit.broadcastMessage(" ");
		}
	}

}
