package com.hythecraft.hythemain.command.commands.chat.directmessage;

import java.util.Arrays;
import java.util.List;

import org.bukkit.entity.Player;

import com.hythecraft.hythemain.HytheCraft;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.bukkit.contexts.OnlinePlayer;

@CommandAlias("message|msg|m|dm|whisper|whisp|w")
public class MessageCommand extends BaseCommand{
	
	//message [player] [message]
	@Default
	@CommandCompletion("@players")
	public void execute(Player player, OnlinePlayer target, String message) {
		Player from = player;
		Player to = target.getPlayer();

		StringBuilder sb = new StringBuilder();
		List<String> st = Arrays.asList(message);
		st.forEach(e -> sb.append(e + " "));
		HytheCraft.getInstance().getPrivetMessageManager().sendMessage(from, to, sb.toString());
		
	}
}
