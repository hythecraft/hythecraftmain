package com.hythecraft.hythemain.command.commands.chat.directmessage;

import java.util.Arrays;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.HytheCraft;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("reply|r")
public class ReplyCommand extends BaseCommand{

	@Default
	public void execute(final CommandSender sender, String args) {
		Player player = (Player)sender;
		
		StringBuilder sb = new StringBuilder();
		List<String> st = Arrays.asList(args);
		st.forEach(e -> sb.append(e + " "));
		HytheCraft.getInstance().getPrivetMessageManager().replyMessage(player, sb.toString());
	}
}
