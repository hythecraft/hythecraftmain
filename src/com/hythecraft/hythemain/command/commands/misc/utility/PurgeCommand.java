package com.hythecraft.hythemain.command.commands.misc.utility;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;

import com.hythecraft.hythemain.HytheCraft;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;

@CommandAlias("purge")
@CommandPermission("hythecraft.admin")
public class PurgeCommand extends BaseCommand{
	
	@Default
	public void execute(CommandSender sender) {
		for(Entity ent : HytheCraft.getDefaultWorld().getEntities())
		{
			if(ent instanceof ArmorStand)
				ent.remove();
		}
	}

}
