package com.hythecraft.hythemain.command.commands.misc.utility;

import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.core.dialogue.util.dialoguetypes.DialogueBase;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.Default;

@CommandAlias("dialu?=diag")
public class DialogueUtilCommand extends BaseCommand{

	public static final String DIAG_CMD = "dialu?=diag";
	
	@Default
	public void execute(CommandSender sender, DialogueBase base, UUID uuid) {
		if(!(HytheCraft.getSessionSecret().equals(uuid)))
			return;
		
		HytheCraft.getInstance().getDialogueManager().startDialogue(base, (Player)sender);
		
	}

}
