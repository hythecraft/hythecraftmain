package com.hythecraft.hythemain.command;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class CommandArchive {
	@Getter static List<String> commandList = new ArrayList<>();
	
	public static void registerCommand(String... names) {
		for(String name : names)
		commandList.add(name);
	}
}
