package com.hythecraft.hythemain.command;

import java.util.UUID;

import com.hythecraft.hythemain.HytheCraft;
import com.hythecraft.hythemain.command.commands.chat.ClearChatCommand;
import com.hythecraft.hythemain.command.commands.chat.directmessage.MessageCommand;
import com.hythecraft.hythemain.command.commands.dialogue.DialogueCommand;
import com.hythecraft.hythemain.command.commands.entity.SpawnCommand;
import com.hythecraft.hythemain.command.commands.fx.particle.ParticleEffectCommand;
import com.hythecraft.hythemain.command.commands.help.HelpCommand;
import com.hythecraft.hythemain.command.commands.help.ResourceCommand;
import com.hythecraft.hythemain.command.commands.help.RpgCommand;
import com.hythecraft.hythemain.command.commands.items.GiveItemCommand;
import com.hythecraft.hythemain.command.commands.items.modifier.ModifierCommand;
import com.hythecraft.hythemain.command.commands.misc.utility.DialogueUtilCommand;
import com.hythecraft.hythemain.command.commands.misc.utility.PurgeCommand;
import com.hythecraft.hythemain.command.commands.party.PartyCommand;
import com.hythecraft.hythemain.command.commands.permission.PermissionCommand;
import com.hythecraft.hythemain.command.commands.quest.QuestCommand;
import com.hythecraft.hythemain.command.commands.trade.TradeCommand;
import com.hythecraft.hythemain.command.commands.user.CharacterCommand;
import com.hythecraft.hythemain.command.commands.user.SaveDataCommand;
import com.hythecraft.hythemain.command.commands.webhook.WebhookCommand;
import com.hythecraft.hythemain.core.dialogue.util.DialogueManager;
import com.hythecraft.hythemain.core.dialogue.util.dialoguetypes.DialogueBase;
import com.hythecraft.hythemain.core.entity.HytheEntity.HytheEntityType;
import com.hythecraft.hythemain.core.items.CustomItemType;
import com.hythecraft.hythemain.core.permission.PermissionGroup;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.PaperCommandManager;
import lombok.Getter;

public class CommandManager {

	@Getter public static PaperCommandManager instance;
	
	
	public CommandManager() {
		instance = new PaperCommandManager(HytheCraft.getInstance());
		//Contexts
		instance.getCommandContexts().registerContext(DialogueBase.class, c -> {return HytheCraft.getInstance().getDialogueManager().getDialogue(c.popFirstArg());});
		instance.getCommandContexts().registerContext(UUID.class, c -> {return UUID.fromString(c.popFirstArg());});
		
		//Tab Completions
		instance.getCommandCompletions().registerAsyncCompletion("dialogues", c -> {return DialogueManager.getDialogueRegistry().keySet();});
		instance.getCommandCompletions().registerAsyncCompletion("hythe-entity", c -> {return HytheEntityType.getAllList();});
		instance.getCommandCompletions().registerAsyncCompletion("hythe-items", c -> {return CustomItemType.getAllList();});
		instance.getCommandCompletions().registerAsyncCompletion("hythe-perm-groups",  c -> {return PermissionGroup.getAllList();});
		//Commands
		registerCommand(
				//Chat
				new MessageCommand(),
				new ClearChatCommand(),
				
				//Dialogue
				new DialogueCommand(),
				
				//Entity
				new SpawnCommand(),
				
				//Fx
				new ParticleEffectCommand(),
				
				//Help
				new HelpCommand(),
				new ResourceCommand(),
				new RpgCommand(),
				
				//Items
				new ModifierCommand(),
				new GiveItemCommand(),
				
				//Misc
				new DialogueUtilCommand(),
				new PurgeCommand(),
				
				//Party
				new PartyCommand(),
				
				//Permission
				new PermissionCommand(),
				
				//Quest
				new QuestCommand(),
				
				//Trade
				new TradeCommand(),
				
				//User
				new CharacterCommand(),
				new SaveDataCommand(),
				
				//Webhook
				new WebhookCommand()

		);
	}
	
	private void registerCommand(BaseCommand... cmds) {
		for(BaseCommand cmd : cmds)
		{
			instance.registerCommand(cmd);
		}
	}

}




















